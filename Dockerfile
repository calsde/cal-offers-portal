# start from base
FROM ubuntu:18.04
MAINTAINER Joshua Ramsamooj

# install system-wide deps
RUN apt-get -yqq update
RUN apt-get -yqq install python3-pip python3-dev curl gnupg
RUN pip3 install --upgrade pip
RUN apt-get install -y git

# copy our application code
RUN mkdir -p /home/apps/tap
COPY . /home/apps/tap
WORKDIR /home/apps/tap

# install requirements
RUN pip3 install -r requirements.txt

# install private requirements
ARG USERNAME
ARG PASSWORD
RUN pip3 install git+https://${USERNAME}:${PASSWORD}@bitbucket.org/calsde/amadeusapiservice/src/master/ --upgrade

