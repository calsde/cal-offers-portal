import redis

AmadeusCreds = {
    'password': 'AmaWeb19',
    'username': 'WSBWGEN',
    'oid': 'POSBW0705'
}

def get_default_currency(oid):
    mapping = {
        'POS': 'TTD',
        'YYZ': 'CAD',
        'MIA': 'USD',
        'JFK': 'USD',
        'ANU': 'XCD',
        'BGI': 'BBD',
        'NAS': 'USD',
        'GND': 'XCD',
        'GEO': 'USD',
        'KIN': 'JMD',
        'SLU': 'XCD',
        'PBM': 'USD',
        'SXM': 'USD',
        'USA': 'USD',
        'SVD': 'USD',
    }

    city = oid[:3]
    if city not in mapping:
        raise KeyError(f'Default currency missing for {city.upper()}')

    return mapping[city]


class DevelopmentConfig:

    ETKT_FAIL_QUEUE = {
        'qnumber': 32,
        'qcategory': 1,
        'oid': 'POSBW0980'
    }

    DEFAULT_CURRENCY = lambda self, oid: get_default_currency(oid)

    AMADEUS1 = {  # todo revert to this version of AMADEUS
        'host_url': 'https://nodeA3.test.webservices.amadeus.com/',
        'wsap': '1ASIWADLBW',
        'duty_code': 'GS',
        'organization': 'BW-AIDL'
    }

    AMADEUS = {
        'host_url': 'https://nodeA1.test.webservices.amadeus.com/',
        'wsap': '1ASIWGENBW',
        'duty_code': 'GS',
        'organization': 'BW'
    }

    INSTANT_SEARCH_WSAP = '1ASIWINSBW_PROD'

    WBS_ACTIONS = {
        'Service_IntegratedPricing': 'TPISGQ_17_1_1A',
        'Service_IntegratedCatalogue': 'TPICGQ_17_1_1A'
    }

    CUSTOM_REMARKS_ID = 'J890C'

    # Group Related
    GROUP_PRIVATE_FARES = True

    GROUP_FOP_AC = '25000070'

    GROUP_DOWNPAYMENT_LIMIT = 1

    DOMESTIC_CITIES = ['POS', 'TAB']

    DOMESTIC_DEPOSIT = 0

    NORTH_AMERICAN_CITIES = ['MIA', 'JFK', 'FLL', 'MCO', 'YYZ']

    NORTH_AMERICAN_DEPOSIT = 100

    CARIBBEAN_DEPOSIT = 50

    NO_DISCOUNT_CLASSES = ['U', 'X', 'S', 'J', 'D', 'C', 'Z', 'I']

    __USER = 'calsde@sdeapps'
    __PASSWORD = '1q2w3e$R%T'
    __DATABASE = 'tap'
    __HOST = 'sdeapps.mysql.database.azure.com'
    __SSL = {'ssl': {'ca': 'cert/BaltimoreCyberTrustRoot.crt.pem'}}

    SQL_ALCHEMY_CONN = 'mysql+pymysql://' + __USER + ':' + __PASSWORD + '@' + __HOST + '/' + __DATABASE

    SESSION_TTL = 120  # minutes

    LSS_PASSWORD_RESET_LINK = "https://pdt.accounts.amadeus.com/LoginService/authorizeAngular?service=lssreset#/forgotPassword"

    REDIS = redis.Redis(
                host="calweb.redis.cache.windows.net",
                port=6380,
                password="ev1DMu0VbghAMrPGvEFscREuA6d4rd0OVY51RJw+vGk=",
                ssl=True
            )


class ProductionConfig:

    ETKT_FAIL_QUEUE = {
        'qnumber': 32,
        'qcategory': 1,
        'oid': 'POSBW0980'
    }

    DEFAULT_CURRENCY = lambda self, oid: get_default_currency(oid)

    AMADEUS = {
        'host_url': 'https://nodeA3.test.webservices.amadeus.com/',
        'wsap': '1ASIWADLBW',
        'duty_code': 'GS',
        'organization': 'BW-AIDL'
    }

    WBS_ACTIONS = {
        'Service_IntegratedPricing': 'TPISGQ_15_1_1A',
        'Service_IntegratedCatalogue': 'TPICGQ_16_1_1A'
    }

    INSTANT_SEARCH_WSAP = '1ASIWINSBW_PROD'

    CUSTOM_REMARKS_ID = 'J890C'

    # Group Related
    GROUP_PRIVATE_FARES = True

    GROUP_FOP_AC = '25000070'

    GROUP_DOWNPAYMENT_LIMIT = 1

    DOMESTIC_CITIES = ['POS', 'TAB']

    DOMESTIC_DEPOSIT = 0

    NORTH_AMERICAN_CITIES = ['MIA', 'JFK', 'FLL', 'MCO', 'YYZ']

    NORTH_AMERICAN_DEPOSIT = 100

    CARIBBEAN_DEPOSIT = 50

    __USER = 'calsde@sdeapps'
    __PASSWORD = '1q2w3e$R%T'
    __DATABASE = 'tap'
    __HOST = 'sdeapps.mysql.database.azure.com'
    __SSL = {'ssl': {'ca': 'cert/BaltimoreCyberTrustRoot.crt.pem'}}

    SQL_ALCHEMY_CONN = 'mysql+pymysql://' + __USER + ':' + __PASSWORD + '@' + __HOST + '/' + __DATABASE

    SESSION_TTL = 15  # minutes

    LSS_PASSWORD_RESET_LINK = "https://www.accounts.amadeus.com/LoginService/authorizeAngular?service=lssreset#/forgotPassword"

    REDIS = redis.Redis(
            host="calweb.redis.cache.windows.net",
            port=6380,
            password="ev1DMu0VbghAMrPGvEFscREuA6d4rd0OVY51RJw+vGk=",
            ssl=True
        )

config_by_name = dict(
    dev=DevelopmentConfig(),
    prod=ProductionConfig()
)

ENV = 'dev'
config = config_by_name[ENV]
