from ..config import config
from AmadeusAPIService.utils.services import Services


class Helpers(Services):

    REMARK_DELIMITER = '***'

    @staticmethod
    def compress_ff_info(fare_family_info):
        """Fare Basis list to string"""
        output = ''
        for st, info in fare_family_info.items():
            output += f'{st}.{info["rbd"]}.{info["fare_basis"]}-'
        return output[:-1]

    @staticmethod
    def decompress_ff_info(fare_family_string):
        """Fare Basis string to list"""

        fare_family_info = {}
        fbs = fare_family_string.split('-')
        for fb in fbs:
            st, rbd, fb = fb.split('.')
            fare_family_info[st] = {
                'fare_basis': fb,
                'rbd': rbd
            }
        return fare_family_info

    def create_custom_remarks(self, remarks):
        """
        List of the following:
        {
            "key": "CUSTOM KEY",
            "value": "Some remark"
        }
        """
        remarks_to_add = []
        for remark in remarks:
            remarks_to_add.append({
                'type': 'RM',
                'remark': f'{config.CUSTOM_REMARKS_ID}{self.REMARK_DELIMITER}{remark["key"]}{self.REMARK_DELIMITER}{remark["value"]}'
            })
        return remarks_to_add

    def get_custom_remarks(self, PNR, required_keys: list = None):
        if len(set(required_keys)) < len(required_keys):
            raise Exception('Unique keys required')

        remarks = {}
        for element in self._listify(PNR['Body']['PNR_Reply']['dataElementsMaster']['dataElementsIndiv']):
            if self.dict_get(element, ['extendedRemark', 'structuredRemark', 'type']) == 'RM':
                text = self.dict_get(element, ['extendedRemark', 'structuredRemark', 'freetext'], '')
                if config.CUSTOM_REMARKS_ID in text:
                    rid, key, value = text.split(self.REMARK_DELIMITER)
                    if required_keys:
                        if key in required_keys:
                            remarks[key] = value
                            required_keys.remove(key)
                    else:
                        remarks[key] = value

        if required_keys:
            raise Exception(f'Custom Remarks missing for {", ".join(required_keys)}')

        return remarks

    def ffi_by_fi(self, fare_family_info, flights):
        """get fare family info by flight info"""

        fare_family_info_by_flight = {}
        for flight in flights:
            for sr, flight_info in flight.items():
                key = f"{flight_info['departure_city']}{flight_info['arrival_city']}{flight_info['flight_datetime']['departure_date']}"
                fare_family_info_by_flight[key] = fare_family_info[sr]

        return fare_family_info_by_flight

    def create_ffi(self, fare_family_info_by_flight, parsed_pnr):
        """creates fare family info using the segment tattoos returned from Amadeus when PNR is created"""
        fare_family_info = {}
        for st, segment in parsed_pnr['segments'].items():
            key = f"{segment['origin']}{segment['destination']}{segment['deptdate']}"
            fare_family_info[st] = fare_family_info_by_flight[key]
        return fare_family_info

if __name__ == '__main__':
    h = Helpers()
    c = h.compress_ff_info({})
    print(c)
    ffi = h.decompress_ff_info(c)
    print(ffi)