import logging
import os
from ..api import ROOT_DIRECTORY

"""
DEBUG	    Detailed information, typically of interest only when diagnosing problems.
INFO	    Confirmation that things are working as expected.
WARNING	    An indication that something unexpected happened, or indicative of some problem in the near future (e.g. ‘disk space low’). The software is still working as expected.
ERROR	    Due to a more serious problem, the software has not been able to perform some function.
CRITICAL	A serious error, indicating that the program itself may be unable to continue running.
"""


def operation_logger(logger_name, credentials: dict = None):
    logger_name = logger_name + '[OPERATION]'

    # get logger
    logger = logging.getLogger(logger_name)

    # set logger level
    logger.setLevel(logging.INFO)

    if logger.handlers:
        return logger

    # create file handler
    if credentials:
        file_formatter = logging.Formatter('%(asctime)s || %(name)s || line %(lineno)d || USERNAME: ' + credentials['username']+ ' || OID: '+credentials['oid']+' || %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    else:
        file_formatter = logging.Formatter('%(asctime)s || %(name)s || line %(lineno)d || %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    # check if folder exist for that OID; create if not
    LOC = os.path.join(ROOT_DIRECTORY, 'logs', credentials['oid'], 'operation')
    FILE = os.path.join(ROOT_DIRECTORY, 'logs', credentials['oid'], 'operation', 'operation-log.log')

    if not os.path.exists(LOC):
        # make dir
        os.makedirs(LOC)
        # make file
        open(FILE, 'a').close()

    file_handler = logging.FileHandler(FILE)
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(file_formatter)

    # create stream handler
    if credentials:
        stream_formatter = logging.Formatter('%(asctime)s || %(name)s || line %(lineno)d || USERNAME: ' + credentials['username']+ ' || OID: '+credentials['oid']+' || %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    else:
        stream_formatter = logging.Formatter('%(asctime)s || %(name)s || line %(lineno)d || %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.INFO)
    stream_handler.setFormatter(stream_formatter)

    # add handlers to logger
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    return logger


def error_logger(logger_name, credentials: dict = None):
    logger_name = logger_name+'[ERROR]'

    # get logger
    logger = logging.getLogger(logger_name)

    # set logger level
    logger.setLevel(logging.ERROR)

    if logger.handlers:
        return logger

    # create file handler
    if credentials:
        file_formatter = logging.Formatter('%(asctime)s || %(name)s || line %(lineno)d || USERNAME: ' + credentials['username'] +' || OID: ' + credentials['oid'] + ' || %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    else:
        file_formatter = logging.Formatter('%(asctime)s || %(name)s || line %(lineno)d || %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    # check if folder exist for that OID; create if not
    LOC = os.path.join(ROOT_DIRECTORY, 'logs', credentials['oid'], 'error')
    FILE = os.path.join(ROOT_DIRECTORY, 'logs', credentials['oid'], 'error', 'error-log.log')

    if not os.path.exists(LOC):
        # make dir
        os.makedirs(LOC)
        # make file
        open(FILE, 'a').close()

    file_handler = logging.FileHandler(FILE)
    file_handler.setLevel(logging.ERROR)
    file_handler.setFormatter(file_formatter)

    # create stream handler
    if credentials:
        stream_formatter = logging.Formatter('%(asctime)s || %(name)s || line %(lineno)d || USERNAME: ' + credentials['username'] +' || OID: ' + credentials['oid'] + ' || %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    else:
        stream_formatter = logging.Formatter('%(asctime)s || %(name)s || line %(lineno)d || %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.ERROR)
    stream_handler.setFormatter(stream_formatter)

    # add handlers to logger
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    return logger

if __name__ == '__main__':
    cred = {
        'username': 'JRAM',
        'oid': 'POSBW0709'
    }
    error_logger(__name__, credentials=cred).error('test')