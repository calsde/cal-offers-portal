from functools import wraps
from flask import session


def pnr_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        try:
            if 'record_locator' not in session:
                return {
                           'status': False,
                           'message': 'PNR not retrieved'
                       }, 401
            return f(*args, **kwargs)
        except Exception as e:
            return {
                       'status': False,
                       'message': str(e)
                   }, 500

    return decorated


def required_in_session(*oargs):
    def decorate(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            try:
                for arg in oargs:
                    if arg not in session.keys():
                        return {
                            'status': False,
                            'message': f'{arg} required in session'
                        }
                return f(*args, **kwargs)
            except Exception as e:
                return {
                           'status': False,
                           'message': str(e)
                       }, 500
        return decorated
    return decorate


def login_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        try:
            if 'logged_in' not in session:
                return {
                           'status': False,
                           'message': 'Login required'
                       }, 401
            return f(*args, **kwargs)
        except Exception as e:
            return {
                       'status': False,
                       'message': str(e)
                   }, 500

    return decorated
