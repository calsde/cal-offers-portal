from flask import Flask, jsonify, request
from flask_restful import Api
from flask_session import Session
from flask_cors import CORS
from .config import config
from .api import *
from .api.Air import *
from .api.Authentication import *
from .api.SSR import *
from .api.Ticket import *
from .api.PNR import *
from .api.Inventory import *
from .api.Pricing import *
from .api.Pay import *
from .api.Loyalty import *
from .api.SVC import *
from .api.Queue import *
from .api.Group import *

# app = Flask(__name__, static_folder="../frontend")
app = Flask(__name__, static_folder="../static", template_folder="../templates", )
SESSION_TYPE = 'redis'
PRESERVE_CONTEXT_ON_EXCEPTION = False
SESSION_REDIS = config.REDIS
app.config.from_object(__name__)
# app.config['SECRET_KEY'] = 'gertq34ct'
Session(app)
CORS(app)


@app.before_request
def before_request():
    if request.path != '/session/info':
        # store session timeout timestamp
        session['session_timeout'] = datetime.datetime.utcnow() + datetime.timedelta(minutes=config.SESSION_TTL)

        session.permanent = True
        app.permanent_session_lifetime = datetime.timedelta(minutes=config.SESSION_TTL)
        session.modified = True


@app.errorhandler(429)
def ratelimit_handler():
    return jsonify({
        'status': False,
        'message': 'Number of attempts exceeded'
    })


api = Api(app)

# Handover to frontend
api.add_resource(Handover, '/')

# Session Information
api.add_resource(SessionInformation, '/session/info')
api.add_resource(SessionReset, '/session/reset')
api.add_resource(DefaultCurrency, '/session/get_default_currency')

# Authentication
api.add_resource(Login, '/auth/login')
api.add_resource(Logout, '/auth/logout')
api.add_resource(GetResetLink, '/auth/reset_link')

# Air
api.add_resource(Availability, '/air/availability')
api.add_resource(DomesticAvailability, '/air/availability/domestic')
api.add_resource(SeatMap, '/air/seatmap')

# SVC
api.add_resource(AddNMCHPenalty, '/svc/add_nmch')
api.add_resource(AddSVC, '/svc/add')

# SSR
api.add_resource(AssignSeats, '/ssr/assign_seats')
api.add_resource(AddServices, '/ssr/add_services')
api.add_resource(GetCatalogue, '/ssr/catalogue')
api.add_resource(SellServices, '/ssr/sell_services')
api.add_resource(AssignedSeats, '/ssr/get_assigned_seats')
api.add_resource(AssignedSeatsIndexed, '/ssr/get_assigned_seats_indexed')
api.add_resource(AssignedServices, '/ssr/get_assigned_services')
api.add_resource(AssignedServicesIndexed, '/ssr/get_assigned_services_indexed')

# PNR
api.add_resource(CreatePNR, '/pnr/create')
api.add_resource(RetrievePNR, '/pnr/retrieve')
api.add_resource(AddBillingAddress, '/pnr/add_billing_address')
api.add_resource(AddFareEndorsement, '/pnr/add_fare_endorsement')
api.add_resource(ClosePNR, '/pnr/close')
api.add_resource(CancelElement, '/pnr/cancel_element')
api.add_resource(CancelElements, '/pnr/cancel_elements')
api.add_resource(CancelItinerary, '/pnr/cancel_itinerary')
api.add_resource(PNRSearch, '/pnr/surname_search')
api.add_resource(UpdateName, '/pnr/update_name')
api.add_resource(AddDocuments, '/pnr/add_documents')
api.add_resource(AddEmergencyContact, '/pnr/add_emergency_contact')
api.add_resource(AddFFNum, '/pnr/add_ffnum')
api.add_resource(RetrieveTSMS, '/pnr/get_all_tsms')
api.add_resource(RetrieveTSTS, '/pnr/get_all_tsts')
api.add_resource(RetrieveTSMP, '/pnr/get_tsmp')
api.add_resource(UpdateTSMPPrice, '/pnr/update_tsmp_price')

# Pricing
api.add_resource(PricePNR, '/pricing/price_pnr')
api.add_resource(InformativePrice, '/pricing/informative')
api.add_resource(FareRules, '/pricing/get_fare_rules')

# PAY
api.add_resource(CreditCardPayment, '/payment/cc')

# Loyalty
api.add_resource(LoyaltyProfile, '/loyalty/get_profile')

# Ticketing
api.add_resource(IssueEMD, '/ticketing/issue_emd')
api.add_resource(IssueETKT, '/ticketing/issue_etkt')
api.add_resource(VoidDocument, '/ticketing/void_doc')
api.add_resource(VoidAllDocument, '/ticketing/void__all_docs')

# Queue
api.add_resource(PlacePNR, '/queue/place_pnr')

# Group
api.add_resource(BookingPossibility, '/group/check_booking_possibility')
api.add_resource(InformativeGroupPrice, '/group/price/informative')
api.add_resource(ReduceGroupSize, '/group/reduce_size')
api.add_resource(IncreaseGroupSize, '/group/increase_size')
api.add_resource(CreateGroup, '/group/create')
api.add_resource(AddPax, '/group/add_pax')
api.add_resource(PriceGroupBooking, '/group/price')
api.add_resource(ValidateDeposit, '/group/validate_deposit')
api.add_resource(ProcessDeposit, '/group/process_deposit')
api.add_resource(AddGroupFOP, '/group/add_fop')
api.add_resource(CreateGroupTST, '/group/create_tst')
api.add_resource(IssueDepoEMD, '/group/issue_depo_emd')
