from flask_restful import Resource
from AmadeusAPIService import PNR as _PNR
from ...services import wrappers
from flask import session
from ...api.SSR import parsers
from ...services.logger import operation_logger, error_logger
from json import dumps
import traceback
from ...config import config


class GetCatalogue(Resource):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        p = _PNR(credentials=session['amadeus_credentials'], actions=config.WBS_ACTIONS)
        args = parsers.get_catalogue_parser.parse_args()
        try:
            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])
            catalogue = p.integrated_catalogue(pt=[args['pt']], st=[args['st']])
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'Catalogue retrieved for PNR: {session["record_locator"]}')

            return {
                'status': True,
                'catalogue': catalogue
            }
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Failed to retrieve catalogue for PNR: {session["record_locator"]} || ERROR: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            p.end_session()


class AddServices(Resource):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        p = _PNR(credentials=session['amadeus_credentials'])
        args = parsers.add_services_parser.parse_args()
        try:
            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])
            p.add_services(args['services'])
            p.rf_er(session['amadeus_credentials']['oid'] + session['amadeus_credentials']['username'])

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'Services git added successfully || PNR: {session["record_locator"]} || SERVICES: {dumps(args)}'
            )
            return {
                'status': True
            }
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Failed to add services || PNR: {session["record_locator"]} || SERVICES: {dumps(args)} || ERROR: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            p.end_session()


class AssignSeats(Resource):
    """ assign seat to passenger """

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        p = _PNR(credentials=session['amadeus_credentials'])
        args = parsers.add_seats_parser.parse_args()
        try:
            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])
            p.assign_seats(args['seat_assignment'])
            p.rf_er(session['amadeus_credentials']['oid'] + session['amadeus_credentials']['username'])

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'Seats assigned successfully || PNR: {session["record_locator"]} || SEATS: {dumps(args)}'
            )
            return {
                'status': True
            }
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Failed to assign seats || PNR: {session["record_locator"]} || SEATS: {dumps(args)} || ERROR: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            p.end_session()


class SellServices(Resource):
    """ price services, create TSMs (seats and services)"""

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):

        p = _PNR(credentials=session['amadeus_credentials'], actions=config.WBS_ACTIONS)
        args = parsers.sell_services_parser.parse_args()

        try:
            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])
            p.price_ssr(pos=args['pos'])
            tsms = p.create_tsm_from_pricing()
            print(tsms)
            p.rf_er(session['amadeus_credentials']['oid'] + session['amadeus_credentials']['username'])

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'TSM(s) created successfully || PNR: {session["record_locator"]} || REQUEST: {dumps(args)}'
            )
            return {
                'status': True,
                'tsm': tsms
            }
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Failed to create TSM(s) || PNR: {session["record_locator"]} || REQUEST: {dumps(args)} || ERROR: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            p.end_session()


class AssignedSeats(Resource):
    """ get seats assigned per pax per segment"""

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):

        p = _PNR(credentials=session['amadeus_credentials'])

        try:
            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])
            assignment = p.get_assigned_seats()
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'Assigned seats retrieved || PNR: {session["record_locator"]} || ASSIGNMENT: {assignment}'
            )
            return {
                'status': True,
                'assignment': assignment
            }
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Failed retrieve assigned seats || PNR: {session["record_locator"]} || ERROR: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            p.end_session()


class AssignedSeatsIndexed(Resource):
    """ get seats assigned per pax indexed by pax tattoo and ssr tattoo, segment information is containted in the ssr object"""

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):

        p = _PNR(credentials=session['amadeus_credentials'])

        try:
            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])
            assignment = p.get_assigned_seats()
            flat_assignment = {}
            for pax_tattoo in assignment:
                for segment in assignment[pax_tattoo]:
                    seat = assignment[pax_tattoo][segment]
                    flat_assignment[seat['tattoo']] = seat
                    flat_assignment[seat['tattoo']]['type'] = 'RQST'
                    flat_assignment[seat['tattoo']]['segment'] = segment
                    flat_assignment[seat['tattoo']]['passenger'] = pax_tattoo

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'Assigned seats (flat) retrieved || PNR: {session["record_locator"]} || ASSIGNMENT: {assignment}'
            )
            return {
                'status': True,
                'assignment': flat_assignment,
            }
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Failed retrieve assigned seats (flat) || PNR: {session["record_locator"]} || ERROR: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            p.end_session()


class AssignedServices(Resource):
    """ get services assigned per pax per segment"""

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):

        p = _PNR(credentials=session['amadeus_credentials'])

        try:
            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])
            assignment = p.get_assigned_services()
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'Assigned services retrieved || PNR: {session["record_locator"]} || ASSIGNMENT: {assignment}'
            )
            return {
                'status': True,
                'assignment': assignment
            }
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Failed retrieve assigned services || PNR: {session["record_locator"]} || ERROR: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            p.end_session()


class AssignedServicesIndexed(Resource):
    """ get services assigned per pax indexed by pax tattoo and ssr tattoo, segment information is containted in the ssr object"""

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        
        p = _PNR(credentials=session['amadeus_credentials'])

        try:
            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])
            assignment = p.get_assigned_services()
            flat_assignment = {}
            for pax_tattoo in assignment:
                for segment in assignment[pax_tattoo]:
                    for service in assignment[pax_tattoo][segment]:
                        flat_assignment[service['tattoo']] = service
                        flat_assignment[service['tattoo']]['segment'] = segment
                        flat_assignment[service['tattoo']]['passenger'] = pax_tattoo

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'Assigned services (flat) retrieved || PNR: {session["record_locator"]} || ASSIGNMENT: {assignment}'
            )
            return {
                'status': True,
                'assignment': flat_assignment,
            }
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Failed retrieve assigned services (flat) || PNR: {session["record_locator"]} || ERROR: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            p.end_session()



