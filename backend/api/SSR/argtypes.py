def services(arg):
    if isinstance(arg, (list, dict)):
        return arg
    else:
        raise TypeError("""
        services should be in a list (for multiple services) or dict (for single services)
        examples:
        {
            "service_type": ""
            "status": ""
            "quantity": ""
            "companyId": ""
            "indicator": ""
            "boardpoint": ""
            "offpoint": ""
            "freetext": ""
            "ssrb": [{'data': '06E', 'seatType': 'N'}, {'data': '06F', 'seatType': 'N'}]
            "references": [{'qualifier': 'PT', 'number':'1'},
                         {'qualifier': 'PT', 'number':'2'}]
        }

        for multiple, send a list of these objects
        """)

def seat_assignment(arg):
    if isinstance(arg, (list, dict)):
        return arg
    else:
        raise TypeError("""
        assignment should be in a list (for multiple) or dict (for single)
        examples
        {
            'seat': '05A',
            'references': [
                {
                    'qualifier': 'PT',
                    'number':'1'
                },
                {
                    'qualifier': 'ST',
                    'number':'1'
                }
            ]
        }
        for multiple, send a list of these objects
        """)
