from flask_restful import reqparse
from ...api.SSR import argtypes


add_services_parser = reqparse.RequestParser(bundle_errors=True)
add_services_parser.add_argument('services', dest='services', required=True, type=argtypes.services, action='append', help='List of services to be added')


add_seats_parser = reqparse.RequestParser(bundle_errors=True)
add_seats_parser.add_argument('seat_assignment', dest='seat_assignment', required=True, type=argtypes.seat_assignment, action='append', help='List of seats to be assigned')


sell_services_parser = reqparse.RequestParser(bundle_errors=True)
sell_services_parser.add_argument('pos', dest='pos', required=True, type=str, help='Point of Sale')

get_catalogue_parser = reqparse.RequestParser(bundle_errors=True)
get_catalogue_parser.add_argument('pt', dest='pt', required=True, type=str, help='Passenger Tattoo')
get_catalogue_parser.add_argument('st', dest='st', required=True, type=str, help='Segment Tattoo')

