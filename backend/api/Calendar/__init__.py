from flask_restful import Resource
from AmadeusAPIService import InstantSearch
from ...services import wrappers
from ...api.Calendar import parsers
from ...services.logger import error_logger, operation_logger
from json import dumps
from ...config import config


class CalendarAvailability(Resource):

    @wrappers.login_required
    def post(self):
        ins = InstantSearch(credentials_for=config.INSTANT_SEARCH_WSAP)
        args = parsers.calendar_availability_parser.parse_args()
        try:
            calendar_availability = ins.availability_search(
                origin=args['origin'],
                destination=args['destination'],
                dept_date=args['dept_date']
            )

            operation_logger(__name__).info(f'Calendar Availability Retrieved')

            return {
                       'status': True,
                       'availability': calendar_availability
                   }, 200

        except Exception as e:
            error_logger(__name__).error(f'Failed to retrieve Calendar Availability || REQUEST: {dumps(args)} || ERROR: {e}')

            return {
                       'status': False,
                       'message': str(e)
                   }, 400
