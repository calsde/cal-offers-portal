from flask_restful import reqparse

calendar_availability_parser = reqparse.RequestParser(bundle_errors=True)
calendar_availability_parser.add_argument('origin', dest='origin', required=True, type=str)
calendar_availability_parser.add_argument('destination', dest='destination', required=True, type=str)
calendar_availability_parser.add_argument('dept_date', dest='dept_date', required=True, type=str)
