from flask_restful import reqparse

flight_loads_parser = reqparse.RequestParser(bundle_errors=True)
flight_loads_parser.add_argument('dept_date', dest='dept_date', required=True, type=str, help='Flight departure date DDMMYYYY')
flight_loads_parser.add_argument('flight_number', dest='flight_number', required=True, type=str, help='Flight number e.g. 600')
flight_loads_parser.add_argument('airline_code', dest='airline_code', required=False, type=str, help='Airline company code', default='BW')
