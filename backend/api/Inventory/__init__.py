from flask_restful import Resource
from AmadeusAPIService import Inventory as _Inventory
from ...services import wrappers
from flask import session
from ...api.Inventory import parsers
from ...services.logger import error_logger, operation_logger
from json import dumps
import traceback


class FlightLoads(Resource):

    @wrappers.login_required
    def post(self):
        inv = _Inventory(credentials=session['amadeus_credentials'])
        args = parsers.flight_loads_parser.parse_args()
        try:
            inv.start_session(2)
            loads = inv.get_flight_loads(flight_number=args['flight_number'], dept_date=args['dept_date'], airline_code=args['airline_code'])
            response = {
                'status': True,
                'flight_loads': loads,
            }

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(f'Flight loads retrieved || REQUEST: {dumps(args)}')
            return response, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(f'Failed to retrieve flight loads || REQUEST: {dumps(args)} || ERROR: {traceback.format_exc()}')

            return {
                       'status': False,
                       'message': str(e)
                   }, 400
        finally:
            inv.end_session()


