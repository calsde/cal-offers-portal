def fbo(f):
    if isinstance(f, list):
        return f
    else:
        raise TypeError('''
        Expected format:
        [{
            'fare_basis': 'Y',
            'segments': ['1', '2']
        },{
            'fare_basis': 'YTASSA',
            'segments': ['3']
        }]
        ''')


def pax(f):
    if isinstance(f, list):
        return f
    else:
        raise TypeError('''
        Expected format:
        [
            {
                'type': 'P',
                'value': '1'
            },
            {
                'type': 'PA',
                'value': '2'
            },
            {
                'type': 'PI',
                'value': '2'
            }
        ]
        ''')


def tax_exemptions(t):
    if isinstance(t, bool) or isinstance(t, list):
        return t
    else:
        raise TypeError('bool or list expected')

