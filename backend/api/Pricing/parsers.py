from flask_restful import reqparse
from ...api.Pricing import argtypes

price_pnr_parser = reqparse.RequestParser(bundle_errors=True)
price_pnr_parser.add_argument('pricing_arguments', dest='pricing_arguments', required=True, type=dict)

fare_rules_parser = reqparse.RequestParser(bundle_errors=True)
fare_rules_parser.add_argument('fare_basis', dest='fare_basis', required=True, type=str)
fare_rules_parser.add_argument('origin', dest='origin', required=True, type=str)
fare_rules_parser.add_argument('destination', dest='destination', required=True, type=str)

informative_price_parser = reqparse.RequestParser(bundle_errors=True)
informative_price_parser.add_argument('pricing_arguments', dest='pricing_arguments', required=True, type=dict)
informative_price_parser.add_argument('flights', dest='flights', required=True, type=dict, action='append')
informative_price_parser.add_argument('fare_family_info', dest='fare_family_info', required=True, type=dict)

store_fb_parser = reqparse.RequestParser(bundle_errors=True)
store_fb_parser.add_argument('fare_bases', dest='fare_basis', required=True, type=dict, action='append')
