from flask import session
from flask_restful import Resource
from ...services.logger import error_logger, operation_logger
from ...api.Pricing import parsers
from ...services import wrappers
from AmadeusAPIService import PNR as _PNR
from AmadeusAPIService import Fare
from AmadeusAPIService import AvailabilityPricer
import traceback
import json
from ...services.helpers import Helpers


class FareRules(Resource):

    @wrappers.login_required
    def post(self):
        """ crate TST(s) from pricing"""
        f = Fare(credentials=session['amadeus_credentials'])
        args = parsers.fare_rules_parser.parse_args()

        try:
            f.start_session(2)
            fr = f.fare_rules(args['fare_basis'], args['origin'], args['destination'])

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'Fare Rules Retrieved || ARGS: {json.dumps(args)}')

            return {
                'status': True,
                'fare_rules': fr
            }, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Failed to Retrieve Fare Rules || ARGS: {json.dumps(args)} || ERROR: {traceback.format_exc()}')
            return {
                       'status': False,
                       'message': str(e)
                   }, 400
        finally:
            f.end_session()


class PricePNR(Resource, Helpers):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):

        p = _PNR(credentials=session['amadeus_credentials'])
        print(1)
        args = parsers.price_pnr_parser.parse_args()
        try:
            # get pricing arguments
            pricing_arguments = args['pricing_arguments']

            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])

            # get fare family info
            custom_remarks = self.get_custom_remarks(p.PNR, required_keys=['FFI'])
            fare_family_info = self.decompress_ff_info(custom_remarks['FFI'])

            # add fare basis override to pricing arguments
            fbo = p.create_fbo(fare_family_info)
            if 'fbo' not in pricing_arguments.keys():
                pricing_arguments['fbo'] = fbo


            f = p.price_pnr(**pricing_arguments)

            if f['fare_options']:
                raise Exception('Multiple fares found. Ensure pricing arguments are correct.')

            # create tsts
            p.create_tst_from_pricing(list(f['tst'].keys()))

            # end
            p.rf_er(session['amadeus_credentials']['oid'] + session['amadeus_credentials']['username'])

            response = {
                'status': True
            }
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'Successfully priced || PNR: {session["record_locator"]} || PRICING ARGUMENTS: {pricing_arguments}')
            return response, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Pricing Error || PNR: {session["record_locator"]} || ERROR: {traceback.format_exc()}')
            return {
                       'status': False,
                       'message': str(e)
                   }, 400
        finally:
            p.end_session()


class InformativePrice(Resource):

    @wrappers.login_required
    def post(self):
        ap = AvailabilityPricer(credentials=session['amadeus_credentials'])
        args = parsers.informative_price_parser.parse_args()
        try:
            pricing_arguments = args['pricing_arguments']
            flights = args['flights']
            fare_family_info = args['fare_family_info']

            ap.start_session(2)

            fares = ap.informative_pricer(
                flights=flights,
                fare_family_info=fare_family_info,
                pricing_arguments=pricing_arguments
            )

            # add fare family info to session for use later on
            session['pricing_info'] = {
                'flights': flights,
                'fare_family_info': fare_family_info
            }

            # add chosen flights to session
            session['flights'] = flights

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'Informative Pricing || ARGS: {json.dumps(args)}')

            return {
                'status': True,
                'fares': fares
            }, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Informative Pricing Error || ARGS: {json.dumps(args)} || ERROR: {traceback.format_exc()}')
            return {
                       'status': False,
                       'message': str(e)
                   }, 400
        finally:
            ap.end_session()

