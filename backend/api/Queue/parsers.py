from flask_restful import reqparse

place_pnr_parsers = reqparse.RequestParser(bundle_errors=True)
place_pnr_parsers.add_argument('qnumber', dest='qnumber', required=True, type=str, help='Queue Number')
place_pnr_parsers.add_argument('qcategory', dest='qcategory', required=True, type=str, help='Queue Category')
place_pnr_parsers.add_argument('qdaterange', dest='qdaterange', required=False, default=None, type=str, help='Optional Queue Daterange')
