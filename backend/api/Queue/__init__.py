from flask_restful import Resource
from AmadeusAPIService import PNR as _PNR
from ...services import wrappers
from flask import session
from ...api.Queue import parsers
from ...services.logger import operation_logger, error_logger
import json
import traceback


class PlacePNR(Resource):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        p = _PNR(credentials=session['amadeus_credentials'])
        args = parsers.place_pnr_parsers.parse_args()
        try:
            p.start_session(2)
            p.place_pnr(
                qnumber=args['qnumber'],
                qcategory=args['qcategory'],
                qdaterange=args['qdaterange'],
                pnr=session["record_locator"]
            )

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'PNR {session["record_locator"]} added to queue || Queue Info: {json.dumps(args)}')

            return {
                       'status': True
                   }, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Filed to add PNR {session["record_locator"]} to queue || Queue Info: {json.dumps(args)} || Error: {traceback.format_exc()}')
            return {
                       'status': False,
                       'message': str(e)
                   }, 400
        finally:
            p.end_session()


