from flask_restful import reqparse

credit_card_payment_parser = reqparse.RequestParser(bundle_errors=True)
credit_card_payment_parser.add_argument('amount', dest='amount', required=False, default=None, type=str)
credit_card_payment_parser.add_argument('currency', dest='currency', required=False, default=None, type=str)
credit_card_payment_parser.add_argument('card_number', dest='card_number', required=True, type=str, help='credit card number')
credit_card_payment_parser.add_argument('card_expiry', dest='card_expiry', required=True, type=str, help='credit card expiry day MMYY')
credit_card_payment_parser.add_argument('card_holder', dest='card_holder', required=True, type=str, help='name on credit card')
credit_card_payment_parser.add_argument('card_id', dest='card_id', required=False, default=None, type=str, help='cvv number')
credit_card_payment_parser.add_argument('references', dest='references', required=False, default=None, type=dict, action='append',
                                        help='list of references associated with payment transaction. If None, FOP will be associated to entire PNR')
