from flask_restful import Resource
from AmadeusAPIService import PNR as _PNR
from ...services import wrappers
from flask import session
from ...api.Pay import parsers
from ...services.logger import operation_logger, error_logger
import traceback
from ...config import config


class CreditCardPayment(Resource):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        p = _PNR(credentials=session['amadeus_credentials'])
        args = parsers.credit_card_payment_parser.parse_args()
        try:
            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])
            monetary_info = None
            if args['amount']:
                monetary_info = {
                    'currency': args['currency'] or config.DEFAULT_CURRENCY(session['amadeus_credentials']['oid']),
                    'amount': args['amount']
                }
            p.fp_cc(card_number=args['card_number'],
                    card_expiry=args['card_expiry'],
                    card_holder=args['card_holder'],
                    card_id=args['card_id'],
                    references=args['references'],
                    monetary_info=monetary_info)
            p.rf_er(session['amadeus_credentials']['oid'] + session['amadeus_credentials']['username'])
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'Payment Successful || PNR: {session["record_locator"]}')
            return {
                'status': True
            }

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Payment Error || PNR: {session["record_locator"]} || ERROR: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            p.end_session()
