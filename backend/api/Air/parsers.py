from flask_restful import reqparse

availability_parser = reqparse.RequestParser(bundle_errors=True)
availability_parser.add_argument('searches', dest='searches', required=True, type=dict, action='append')
availability_parser.add_argument('pricing_arguments', dest='pricing_arguments', required=True, type=dict)

seat_map_parser = reqparse.RequestParser(bundle_errors=True)
seat_map_parser.add_argument('dept_date', dest='dept_date', required=True, type=str, help='Flight departure date DDMMYY')
seat_map_parser.add_argument('dept_city', dest='dept_city', required=True, type=str, help='Flight departure city e.g. POS')
seat_map_parser.add_argument('arrv_city', dest='arrv_city', required=True, type=str, help='Flight arrival city e.g. YYZ')
seat_map_parser.add_argument('flight_number', dest='flight_number', required=True, type=str, help='Flight number e.g. 600')
seat_map_parser.add_argument('booking_class', dest='booking_class', required=True, type=str, help='Booking class e.g. Y')
seat_map_parser.add_argument('airline_code', dest='airline_code', required=False, default='BW', type=str, help='Airline code. Defaults to BW')
seat_map_parser.add_argument('currency', dest='currency', required=False, default=None, type=str, help='Required currency')
