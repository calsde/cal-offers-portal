from flask_restful import Resource
from AmadeusAPIService import AvailabilityPricer
from AmadeusAPIService import Air as _Air
from ...services import wrappers
from flask import session
from ...api.Air import parsers
from ...services.logger import error_logger, operation_logger
from json import dumps
import traceback


class SeatMap(Resource):

    @wrappers.login_required
    def post(self):
        a = _Air(credentials=session['amadeus_credentials'])
        args = parsers.seat_map_parser.parse_args()
        try:
            a.start_session(2)
            seat_map = a.seat_map(dept_date=args['dept_date'], dept_city=args['dept_city'], arrv_city=args['arrv_city'],
                                  flight_number=args['flight_number'], booking_class=args['booking_class'],
                                  company=args['airline_code'], currency=args['currency'])
            response = {
                'status': True,
                'seat_map': seat_map,
                'seat_characteristics_description': a.SEAT_CHARACTERISTICS,
                'seat_occupation_description': a.SEAT_OCCUPATION
            }

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(f'Seat map retrieved || REQUEST: {dumps(args)}')
            return response, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(f'Failed to retrieve seat map || REQUEST: {dumps(args)} || ERROR: {traceback.format_exc()}')

            return {
                       'status': False,
                       'message': str(e)
                   }, 400
        finally:
            a.end_session()


class Availability(Resource):
    @wrappers.login_required
    def post(self):

        args = parsers.availability_parser.parse_args()
        ap = AvailabilityPricer(credentials=session['amadeus_credentials'])

        try:
            result = ap.search(
                availability_searches=args['searches'],
                pricing_arguments=args['pricing_arguments']
            )

            response = {
                'status': True,
                'result': result
            }
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(f'Availability retrieved successfully || REQUEST: {dumps(args)}')
            return response, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(f'Failed to retrieve availability || REQUEST: {dumps(args)} || ERROR: {traceback.format_exc()}')

            return {
                'status': False,
                'message': str(e)
            }, 400


class DomesticAvailability(Resource):
    @wrappers.login_required
    def post(self):

        args = parsers.availability_parser.parse_args()
        ap = AvailabilityPricer(credentials=session['amadeus_credentials'])

        try:
            result = ap.domestic_search(
                availability_searches=args['searches'],
                pricing_arguments=args['pricing_arguments']
            )

            response = {
                'status': True,
                'result': result
            }
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(f'Domestic availability retrieved successfully || REQUEST: {dumps(args)}')
            return response, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(f'Failed to retrieve domestic availability || REQUEST: {dumps(args)} || ERROR: {traceback.format_exc()}')

            return {
                'status': False,
                'message': str(e)
            }, 400
