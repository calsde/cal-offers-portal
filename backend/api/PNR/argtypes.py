def passengers(p):
    if isinstance(p, (list, dict)):
        return p
    else:
        raise TypeError('passengers should be in a list (for multiple passengers) or dict (for single passenger)')


def fqtv(p):
    if isinstance(p, dict):
        if 'company_id' not in p:
            p['company_id'] = 'BW'

        return p
    else:
        raise TypeError('dict expected')

def cancel_elements(arg):
    if isinstance(arg, (list,dict)):
        return arg
    else:
        raise TypeError("""
        Elements to cancel should be a list in the following format
        [
			{
				"type": "PT",
				"number": "7"
			},
			{
				"type": "PT",
				"number": "5"
			}
		]
    """)



