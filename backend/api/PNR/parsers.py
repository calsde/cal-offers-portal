from flask_restful import reqparse
from ...api.PNR import argtypes

update_name_parser = reqparse.RequestParser(bundle_errors=True)
update_name_parser.add_argument('tattoo', dest='tattoo', required=True, type=str, help='Pax tattoo')
update_name_parser.add_argument('new_fname', dest='new_fname', required=True, type=str, help='Desired first name')
update_name_parser.add_argument('new_lname', dest='new_lname', required=True, type=str, help='Desired last name')

cancel_elements_parser = reqparse.RequestParser(bundle_errors=True)
cancel_elements_parser.add_argument('elements', dest='elements', required=True, action='append',  type=argtypes.cancel_elements, help='List of elements to cancel')


cancel_element_parser = reqparse.RequestParser(bundle_errors=True)
cancel_element_parser.add_argument('element_identifier', dest='element_identifier', required=True, type=str, help='e.g. PT, ST, OT etc; tattoo type')
cancel_element_parser.add_argument('number', dest='number', required=True, type=str, help='tattoo number')

pnr_search = reqparse.RequestParser(bundle_errors=True)
pnr_search.add_argument('last_name', dest='last_name', required=True, type=str)

add_ffnum_parser = reqparse.RequestParser(bundle_errors=True)
add_ffnum_parser.add_argument('ffnum', dest='ffnum', required=True, type=str, help='Frequent Flyer Number')
add_ffnum_parser.add_argument('pt', dest='pt', required=False, default=None, type=str, help='Passenger Tattoo')

retireve_parser = reqparse.RequestParser(bundle_errors=True)
retireve_parser.add_argument('record_locator', dest='record_locator', required=True, type=str, help='Record Locator')

retrieve_tsmp_parser = reqparse.RequestParser(bundle_errors=True)
retrieve_tsmp_parser.add_argument('tattoo', dest='tattoo', required=True, type=str, help='TSMP Tattoo')

update_tsmp_price_parser = reqparse.RequestParser(bundle_errors=True)
update_tsmp_price_parser.add_argument('tsmp_tattoo', dest='tsmp_tattoo', required=True, type=str, help="Tattoo of TSMP to update is required")
update_tsmp_price_parser.add_argument('record_locator', dest='record_locator', required=True, type=str, help="Tattoo of record locator is required")

add_emergency_contact_parser = reqparse.RequestParser()
add_emergency_contact_parser.add_argument('name', dest='name', required=True, type=str)
add_emergency_contact_parser.add_argument('country_code', dest='country_code', required=True, type=str)
add_emergency_contact_parser.add_argument('phone_number', dest='phone_number', required=True, type=str)

add_documents_parser = reqparse.RequestParser()
add_documents_parser.add_argument('documents_info', dest='documents_info', required=False, default=None, action='append', type=dict)

builder_parser = reqparse.RequestParser()
builder_parser.add_argument('flights', dest='flights', required=True, action='append', type=dict)
builder_parser.add_argument('pax', dest='pax', required=True, action='append', type=dict)
builder_parser.add_argument('contacts', dest='contacts', required=True, action='append', type=dict)

pnr_add_fe = reqparse.RequestParser()
pnr_add_fe.add_argument('text', dest='text', required=True)
pnr_add_fe.add_argument('references', dest='references', required=False, default=None, action='append', type=dict)

pnr_add_billing_address = reqparse.RequestParser()
pnr_add_billing_address.add_argument('line_1', dest='A1', required=True)
pnr_add_billing_address.add_argument('line_2', dest='A2', required=False, default=None)
pnr_add_billing_address.add_argument('city', dest='CI', required=False, default=None)
pnr_add_billing_address.add_argument('country', dest='CO', required=False, default=None)
pnr_add_billing_address.add_argument('company', dest='CY', required=False, default=None)
pnr_add_billing_address.add_argument('name', dest='NA', required=False, default=None)
pnr_add_billing_address.add_argument('po_box', dest='PO', required=False, default=None)
pnr_add_billing_address.add_argument('state', dest='ST', required=False, default=None)
pnr_add_billing_address.add_argument('postal_code', dest='ZP', required=False, default=None)