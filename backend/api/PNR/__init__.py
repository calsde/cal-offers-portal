from flask_restful import Resource
from AmadeusAPIService import PNR as _PNR
from AmadeusAPIService.utils.errorhandler import NoTSMRecord, NoTSTRecord
from ...services import wrappers
from ...services.helpers import Helpers
from flask import session
from ...api.PNR import parsers
from ...services.logger import operation_logger, error_logger
from json import dumps
from AmadeusAPIService.PNR.pnr_builder import PNRBuilder
from AmadeusAPIService.PNR import PNR
from pprint import pprint
import traceback


class UpdateName(Resource):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        """
        Update Name on PNR
        """
        pnr = PNR(credentials=session['amadeus_credentials'])
        args = parsers.update_name_parser.parse_args()
        try:
            pnr.start_session(2)
            pnr.retrieve_by_rec_loc(session['record_locator'])

            # get current names
            parsed_pnr = pnr.parse_pnr()
            original_fname = parsed_pnr['pax'][args['tattoo']]['fname']
            original_lname = parsed_pnr['pax'][args['tattoo']]['lname']

            # validate name change is allowed
            pnr.name_change_validator(
                original_lname, args['new_lname'], original_fname, args['new_fname'])

            # update name and add OSI
            pnr.update_name(args['new_fname'],
                            args['new_lname'], args['tattoo'])
            pnr.add_osi(
                f"NAME CORRECTION BY {session['amadeus_credentials']['oid'] + session['amadeus_credentials']['username']}")
            pnr.rf_er(session['amadeus_credentials']['oid'] +
                      session['amadeus_credentials']['username'])

            # if ticketed, fee is required
            fee = pnr.check_ticketing()

            response = {
                'status': True,
                'fee': fee
            }

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f"Name changed successfully || PNR: {session['record_locator']} || Request: {args}")
            return response, 200
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f"Name change failed || PNR: {session['record_locator']} || Request: {args} || ERROR: {traceback.format_exc()}")
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            pnr.end_session()


class CancelElements(Resource):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        """
        Cancel Multiple PNR Elements
        """
        pnr = PNR(credentials=session['amadeus_credentials'])
        args = parsers.cancel_elements_parser.parse_args()
        try:
            pnr.start_session(2)
            pnr.retrieve_by_rec_loc(session['record_locator'])
            pnr.pnr_cancel_elements('E', args['elements'])
            pnr.rf_er(session['amadeus_credentials']['oid'] +
                      session['amadeus_credentials']['username'])

            response = {
                'status': True
            }
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f"Elements cancelled successfully || Elements: {args['elements']}")
            return response, 200
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f"Element cancellation unsuccessfully || Element: {args['elements']} || ERROR: {traceback.format_exc()}")
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            pnr.end_session()


class CancelElement(Resource):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        """
        Cancel PNR element
        """
        pnr = PNR(credentials=session['amadeus_credentials'])
        args = parsers.cancel_element_parser.parse_args()
        try:
            pnr.start_session(2)
            pnr.retrieve_by_rec_loc(session['record_locator'])
            pnr.pnr_cancel_element(
                'E', args['element_identifier'], args['number'])
            pnr.rf_er(session['amadeus_credentials']['oid'] +
                      session['amadeus_credentials']['username'])

            response = {
                'status': True
            }
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f"Element cancelled successfully || Element Type: {args['element_identifier']} || Tattoo: {args['number']}")
            return response, 200
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f"Element cancellation unsuccessfully || Element Type: {args['element_identifier']} || Tattoo: {args['number']} || ERROR: {traceback.format_exc()}")
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            pnr.end_session()


class CancelItinerary(Resource):

    """Deletes itinerary and void all documents on the PNR"""

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):

        pnr = PNR(credentials=session['amadeus_credentials'])
        try:
            pnr.start_session(2)
            pnr.retrieve_by_rec_loc(session['record_locator'])
            pnr.cancel_itinerary()
            pnr.rf_er(session['amadeus_credentials']['oid'] +
                      session['amadeus_credentials']['username'])

            response = {
                'status': True
            }

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f"Itinerary cancelled successfully || PNR: {session['record_locator']}")
            return response, 200
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f"Itinerary cancellation failed || PNR: {session['record_locator']} || Error: {traceback.format_exc()} || ERROR: {traceback.format_exc()}")
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            pnr.end_session()


class CreatePNR(Resource, Helpers):

    @wrappers.login_required
    def post(self):
        """
        Create a simple PNR. This includes name, flights, contacts, emergency contact, docs, rf, tkok
        :return:
        """
        args = parsers.builder_parser.parse_args()
        pnrb = PNRBuilder(credentials=session['amadeus_credentials'])
        try:
            existing_pricing_info = session['pricing_info']

            flights = pnrb.search_create_pnr_converter(
                flights=args['flights'],
                fare_family_info=existing_pricing_info['fare_family_info'],
                quantity=len(args['pax'])
            )

            pnrb.start_session(2)
            pnrb.add_flight_segments(flights)
            pnrb.add_contacts(args['contacts'])

            # get dob DOCS info from pax object
            docs = []
            for pax in pnrb._listify(args['pax']):
                docs.append(
                    {
                        'dob': pax['dob'],
                        'fname': pax['first_name'],
                        'lname': pax['last_name'],
                        'gender': pax['gender'],
                        'reference': {'qualifier': 'PR', 'number': pax['identifier']}
                    }
                )

            pnrb.add_pax(args['pax'])
            pnrb.add_docs(docs_info=docs)
            pnrb.tkok()
            pnrb.option_code = 0
            # update fare family info with new ST for use in subsequent pricing

            # get ff info by flight info
            fare_family_info_by_flight = self.ffi_by_fi(
                existing_pricing_info['fare_family_info'], existing_pricing_info['flights'])

            # create PNR
            pnrb.create_pnr()
            parsed_info = pnrb.parse_pnr(pnrb.PNR)
            session['record_locator'] = pnrb.recloc

            # create new fare family info by segment tattoo
            fare_family_info = self.create_ffi(
                fare_family_info_by_flight, parsed_info)

            # Add fare family info to PNR as a remark
            # reset builder
            pnrb.reset_builder()

            # compress fare family info
            compressed_ffi = self.compress_ff_info(fare_family_info)

            # add remark
            remarks = self.create_custom_remarks([
                {
                    "key": "FFI",
                    "value": compressed_ffi
                }
            ])

            pnrb.add_remarks(remarks)
            pnrb.option_code = 11
            pnrb.rf(session['amadeus_credentials']['oid'] +
                    session['amadeus_credentials']['username'])
            pnrb.update_pnr()
            parsed_info = pnrb.parse_pnr(pnrb.PNR)

            # add PNR to session for future use
            session['record_locator'] = pnrb.recloc

            response = {
                'status': True,
                'recloc': pnrb.recloc,
                'parsed_pnr': parsed_info
            }

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'PNR {pnrb.recloc} created successfully.')
            return response, 200
        except Exception as e:
            pprint(pnrb.response)
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Error in creating PNR || REQUEST: {dumps(args)} || ERROR: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            pnrb.end_session()


class AddEmergencyContact(Resource):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        """
        Add Emergency Contact to PNR
        """
        args = parsers.add_emergency_contact_parser.parse_args()
        pnr = PNR(credentials=session['amadeus_credentials'])
        try:
            pnr.start_session(2)
            pnr.retrieve_by_rec_loc(session['record_locator'])
            pnr.add_emergency_contact(
                args['name'], args['country_code'], args['phone_number'])
            pnr.rf_er(session['amadeus_credentials']['oid'] +
                      session['amadeus_credentials']['username'])

            response = {
                'status': True
            }
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'Successfully added emergency contact to PNR {session["record_locator"]} || REQUEST: {dumps(args)}')
            return response, 200
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Error in adding emergency contact to PNR {session["record_locator"]} || REQUEST: {dumps(args)} || ERROR: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            pnr.end_session()


class AddFFNum(Resource):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        """
        Add Frequent Flyer Number to PNR
        :return:
        """
        args = parsers.add_ffnum_parser.parse_args()
        pnr = PNR(credentials=session['amadeus_credentials'])
        try:
            pnr.start_session(2)
            pnr.retrieve_by_rec_loc(session['record_locator'])
            pnr.add_frequent_flyer_number(args['ffnum'], tattoo=args['pt'])
            pnr.rf_er(session['amadeus_credentials']['oid'] +
                      session['amadeus_credentials']['username'])

            response = {
                'status': True
            }
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'Successfully added FF number to PNR {session["record_locator"]} || REQUEST: {dumps(args)}')
            return response, 200
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Error in adding FF Number to PNR {session["record_locator"]} || REQUEST: {dumps(args)} || ERROR: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            pnr.end_session()


class AddDocuments(Resource):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        """
        Add documents to PNR (Passport etc)
        """
        args = parsers.add_documents_parser.parse_args()
        pnr = PNR(credentials=session['amadeus_credentials'])
        try:
            pnr.start_session(2)
            pnr.retrieve_by_rec_loc(session['record_locator'])
            pnr.add_documents(args['documents'])
            pnr.rf_er(session['amadeus_credentials']['oid'] +
                      session['amadeus_credentials']['username'])

            response = {
                'status': True
            }
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'Successfully added documents to PNR {session["record_locator"]} || REQUEST: {dumps(args)}')
            return response, 200
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Error in adding documents to PNR {session["record_locator"]} || REQUEST: {dumps(args)} || ERROR: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            pnr.end_session()


class AddBillingAddress(Resource):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        p = _PNR(credentials=session['amadeus_credentials'])
        args = parsers.pnr_add_billing_address.parse_args()
        try:
            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])
            p.pnr_delete_billing_address()
            p.add_billing_address(
                A1=args['A1'],
                A2=args['A2'],
                CI=args['CI'],
                CO=args['CO'],
                CY=args['CY'],
                NA=args['NA'],
                PO=args['PO'],
                ST=args['ST'],
                ZP=args['ZP']
            )
            p.rf_er(session['amadeus_credentials']['oid'] +
                    session['amadeus_credentials']['username'])

            response = {
                'status': True,
                'message': 'Billing Address added successfully'
            }
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'Billing Address address successfully || PNR: {session["record_locator"]}')
            return response, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Error in adding Billing Address || PNR: {session["record_locator"]}|| REQUEST: {dumps(args)} || ERROR: {traceback.format_exc()}')

            return {
                'status': False,
                'message': str(e)
            }, 400
        finally:
            p.end_session()


class AddFareEndorsement(Resource):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        p = _PNR(credentials=session['amadeus_credentials'])
        args = parsers.pnr_add_fe.parse_args()
        try:
            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])
            p.add_fare_endorsement(
                text=args['text'],
                references=args['references']
            )
            p.rf_er(session['amadeus_credentials']['oid'] +
                    session['amadeus_credentials']['username'])

            response = {
                'status': True,
                'message': 'Fare endorsement successfully'
            }
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'Fare endorsement successfully || PNR: {session["record_locator"]}')
            return response, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Error in adding fare endorsement || PNR: {session["record_locator"]} || REQUEST: {dumps(args)} || ERROR: {traceback.format_exc()}')

            return {
                'status': False,
                'message': str(e)
            }, 400
        finally:
            p.end_session()


class RetrievePNR(Resource):

    @wrappers.login_required
    def post(self):
        print(session)
        p = _PNR(credentials=session['amadeus_credentials'])
        args = parsers.retireve_parser.parse_args()
        try:
            p.start_session(2)
            p.retrieve_by_rec_loc(args['record_locator'])
            parsed_info = p.parse_pnr()
            session['record_locator'] = p.recloc

            response = {
                'status': True,
                'pnr': p.PNR,
                'parsed_pnr': parsed_info
            }
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'PNR {args["record_locator"]} retrieved.')
            return response, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Error in retrieving {args["record_locator"]} || REQUEST: {dumps(args)} || ERROR: {traceback.format_exc()}')

            return {
                'status': False,
                'message': str(e)
            }, 400
        finally:
            p.end_session()


class RetrieveTSMS(Resource):
    @wrappers.login_required
    def post(self):
        p = _PNR(credentials=session['amadeus_credentials'])
        try:
            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])

            session['record_locator'] = p.recloc

            try:
                t = p.get_all_tsms()
            except NoTSMRecord:
                t = []

            response = {
                'status': True,
                'tsms': t,
            }

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'PNR {session["record_locator"]} retrieved.')
            return response, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Error in retrieving {session["record_locator"]} || ERROR: {traceback.format_exc()}')

            return {
                'status': False,
                'message': str(e)
            }, 400
        finally:
            p.end_session()


class RetrieveTSTS(Resource):

    @wrappers.login_required
    def post(self):
        p = _PNR(credentials=session['amadeus_credentials'])
        try:
            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])

            session['record_locator'] = p.recloc

            try:
                t = p.get_all_tsts()
            except NoTSTRecord:
                t = []

            response = {
                'status': True,
                'tsts': t,
            }
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'TSTs for PNR {session["record_locator"]} retrieved.')
            return response, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Error in retrieving TSTs for PNR: {session["record_locator"]} || ERROR: {traceback.format_exc()}')

            return {
                'status': False,
                'message': str(e)
            }, 400
        finally:
            p.end_session()


class UpdateTSMPPrice(Resource):
    """
    UpdateTSMP Pricing of a service (allows custom discounts)
    """
    @wrappers.login_required
    def post(self):
        p = _PNR(credentials=session['amadeus_credentials'])
        args = parsers.update_tsmp_price_parser.parse_args()
        try:
            p.start_session()
            p.retrieve_by_rec_loc(args['record_locator'])

            session['record_locator'] = p.recloc
            
            tsmp = p.get_tsmp(args['tsmp_tattoo'])

            internationalIndicator = {
                'attributeDetails': {
                    'attributeType': 'INT',
                    'attributeDescription': 'I'
                }
            }

            # todo get prices from dw at this point by recloc and lname
            base = 400
            tax = 400*0.125
            total = tax + base

            monInfo = tsmp['monetaryInformation']

            for mDetail in monInfo:
                if mDetail['monetaryDetails']['typeQualifier'] == 'T':
                    mDetail['monetaryDetails']['amount'] = str(total)
                elif mDetail['monetaryDetails']['typeQualifier'] == 'F':
                    mDetail['monetaryDetails']['amount'] = str(base)

            taxInfo = {
                'taxCategory': 'X',
                'taxDetails': {
                    # rate in this case is the actual amount of tax, not a percentage
                    'rate': str(tax),
                    'countryCode': 'TT',  # TODO: get country code from DW
                    'currencyCode': 'TTD',  # TODO: get currency code from DW
                    'type': 'VE',

                }
            }

            mTsmFlags = [{
                'statusDetails': {
                    'indicator': 'CNF',
                    'action': '1'
                }
            },
                {
                'statusDetails': {
                    'indicator': 'IRQ',
                    'action': '1'
                }
            }]

            coupsDets = {
                'serviceTattoo': {
                    'referenceDetails': {
                        'type': 'SSR',
                        'value': tsmp['couponsDetails']['productTattoo']['referenceDetails']['value']
                    },

                },
                'ambiDummy': '',
            }

            p.update_tsmp(
                tattooOfTSM=tsmp['tattooOfTSM'],
                internationalIndicator=internationalIndicator,
                tsmFlags=mTsmFlags,
                monetaryInformation=monInfo,
                bankExchangeRate=tsmp['bankExchangeRate'] if 'bankExchangeRate' in tsmp else None,
                taxInformation=taxInfo,
                freeTextData=tsmp['freeTextData'] if 'freeTextData' in tsmp else None,
                validatingCarrier=tsmp['validatingCarrier'],
                # location=tsm['location'],
                couponsDetails=coupsDets,
                # reasonCode=tsm['reasonCode'],
            )

            p.rf_er('OffersPortal')

            return {
                'status': True,
                'message': 'Service Pricing Updated',
                'tsmpUpdateResult': p.response
            }, 200

        except Exception as e:
            print(str(e))
            return {
                'status': False,
                'message': str(e)
            }, 400
        finally:
            p.end_session()


class RetrieveTSMP(Resource):

    @wrappers.login_required
    def post(self):
        p = _PNR(credentials=session['amadeus_credentials'])
        args = parsers.retrieve_tsmp_parser.parse_args()
        try:
            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])

            session['record_locator'] = p.recloc

            t = p.get_tsmp(args['tattoo'])
            response = {
                'status': True,
                'tsmp': t
            }
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'TSMP {args["tattoo"]} retrieved for {session["record_locator"]}.')
            return response, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Error in retrieving TSMP with tattoo {args["tattoo"]} for PNR {session["record_locator"]} || REQUEST: {dumps(args)} || ERROR: {traceback.format_exc()}')

            return {
                'status': False,
                'message': str(e)
            }, 400
        finally:
            p.end_session()


class ClosePNR(Resource):

    """remove PNR from session context after it has been retrieved or created."""

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        try:
            rl = session['record_locator']
            del session['record_locator']
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'PNR closed || PNR: {rl}')
            return {
                'status': True
            }, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Unable to close PNR || ERROR: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }, 400


class PNRSearch(Resource):

    """Search for PNRs by last name"""

    @wrappers.login_required
    def post(self):
        p = _PNR(credentials=session['amadeus_credentials'])
        args = parsers.pnr_search.parse_args()
        try:
            p.start_session(2)
            pnrs = p.list_pnrs_by_surname(args['last_name'])
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'PNR Search by surname || Surname: {args["last_name"]}')
            return {
                'status': True,
                'pnrs': pnrs
            }, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'PNR Search by surname failed || Surname: {args["last_name"]} || ERROR: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }, 400
        finally:
            p.end_session()
