from flask_restful import reqparse

login_parser = reqparse.RequestParser(bundle_errors=True)
login_parser.add_argument('username', dest='username', required=True, type=str, help='LSS username')
login_parser.add_argument('password', dest='password', required=False, type=str, help='LSS password')
login_parser.add_argument('oid', dest='oid', required=True, type=str, help='Office ID that LSS account is assigned to')

