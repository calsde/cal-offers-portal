from flask_restful import Resource
from AmadeusAPIService.Security import Security
from ...config import config
from flask import session
from ...api.Authentication import parsers
from ...services import wrappers
from ...services.logger import operation_logger, error_logger
import traceback


class Login(Resource):

    def post(self):
        credentials = parsers.login_parser.parse_args()
        s = Security()

        try:
            credentials.update(config.AMADEUS)
            s.validate_account(credentials)

            # store credentials in session for future use in other Amadeus WBS calls.
            session['amadeus_credentials'] = credentials
            session['logged_in'] = True

            operation_logger(__name__, credentials=credentials).info('LOGGED IN')

            return {
                'status': True
            }

        except Exception as e:
            s.end_session()
            error_logger(__name__, credentials=credentials).error(f'LOGIN ERROR: {traceback.format_exc()}')

            return {
                'status': False,
                'message': str(e)
            }


class Logout(Resource):

    @wrappers.login_required
    def post(self):
        credentials = session['amadeus_credentials']
        try:
            session.clear()
            operation_logger(__name__, credentials=credentials).info('LOGGED OUT')
            return {
                'status': True
            }

        except Exception as e:
            error_logger(__name__, credentials=credentials).error(f'LOGOUT ERROR: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }


class GetResetLink(Resource):

    def post(self):
        return {
            'status': True,
            'link': config.LSS_PASSWORD_RESET_LINK
        }
