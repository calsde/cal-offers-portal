from flask_restful import reqparse

add_nmch_parser = reqparse.RequestParser(bundle_errors=True)
add_nmch_parser.add_argument('tattoo', dest='tattoo', required=True, type=str, help='List of services to be added')

add_svc_parser = reqparse.RequestParser(bundle_errors=True)
add_svc_parser.add_argument('quantity', dest='quantity', required=True, type=str, help='The number of instances of the auxiliary segment and can then be higher than the number of passengers in the PNR')
add_svc_parser.add_argument('code', dest='code', required=True, type=str, help='SVC code e.g. NMCH for Name Change')
add_svc_parser.add_argument('city', dest='city', required=True, type=str, help='The city/airport where the service is to be provided')
add_svc_parser.add_argument('date', dest='date', required=True, type=str, help='The reservation date of the auxiliary segment under DDMMYY format')
add_svc_parser.add_argument('references', dest='references', required=False, type=dict, action='append', help='Elements SVC is associated with')
add_svc_parser.add_argument('text', dest='text', required=False, type=str, help='Free text')
add_svc_parser.add_argument('rfic', dest='rfic', required=False, type=str, help='A Reason for Issuance Code')
add_svc_parser.add_argument('rfisc', dest='rfisc', required=False, type=str, help='A Reason for Issuance Sub-code')
