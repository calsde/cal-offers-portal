from flask_restful import Resource
from AmadeusAPIService import PNR as _PNR
from ...services import wrappers
from flask import session
from ...api.SVC import parsers
from ...services.logger import operation_logger, error_logger
import datetime
import json


class AddNMCHPenalty(Resource):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        pnr = _PNR(credentials=session['amadeus_credentials'])
        args = parsers.add_nmch_parser.parse_args()
        try:
            pnr.start_session(2)
            pnr.retrieve_by_rec_loc(session['record_locator'])
            svc = pnr.create_aux_segment('1', 'NMCH', city=session['amadeus_credentials']['oid'][:3],
                                         date=datetime.datetime.now().strftime('%d%m%y'),
                                         pax_reference=[{'type': 'PT', 'value': args['tattoo']}], freetext='NAME CHANGE')

            pnr.rf_er(session['amadeus_credentials']['oid'] + session['amadeus_credentials']['username'])

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'NMCH added || PNR: {session["record_locator"]} || Passenger Tattoo: {args["tattoo"]}')

            return {
                'status': True,
                'svc': svc
            }

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Failed to add NMCH || PNR: {session["record_locator"]} || Passenger Tattoo: {args["tattoo"]}')
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            pnr.end_session()


class AddSVC(Resource):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        pnr = _PNR(credentials=session['amadeus_credentials'])
        args = parsers.add_svc_parser.parse_args()
        try:
            pnr.start_session(2)
            pnr.retrieve_by_rec_loc(session['record_locator'])
            pnr.create_aux_segment(args['quantity'], args['code'], args['city'], args['date'],
                                   pax_reference=args['references'], freetext=args['text'],
                                   rfic=args['rfic'], rfisc=args['rfisc'])

            pnr.rf_er(session['amadeus_credentials']['oid'] + session['amadeus_credentials']['username'])

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'SVC added || PNR: {session["record_locator"]} || SVC Info: {json.dumps(args)}')

            return {
                'status': True,
            }

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Failed to add SVC || PNR: {session["record_locator"]} || SVC Info: {json.dumps(args)}')
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            pnr.end_session()

