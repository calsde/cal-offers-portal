from flask_restful import reqparse

issue_emd_parser = reqparse.RequestParser(bundle_errors=True)
issue_emd_parser.add_argument('tsm', dest='tsm', required=False, default=None, type=int, action='append', help='List of TSMs to be issued')

issue_etkt_parser = reqparse.RequestParser(bundle_errors=True)
issue_etkt_parser.add_argument('tst', dest='tst', required=False, default=None, type=int, action='append', help='List of TSTs to be issued')

auto_update_etkt_parser = reqparse.RequestParser(bundle_errors=True)
auto_update_etkt_parser.add_argument('etkt', dest='etkt', required=True, type=str, help='E-TKT number without the -')
auto_update_etkt_parser.add_argument('references', dest='references', required=True, type=dict, action='append', help='References to seg or pax. See AmadeusAPIService/Ticket/__init__.py')

void_doc_parser = reqparse.RequestParser(bundle_errors=True)
void_doc_parser.add_argument('doc_number', dest='doc_number', required=True, type=str, help='Document number to be voided (without the -)')
