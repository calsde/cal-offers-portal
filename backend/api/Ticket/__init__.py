from flask_restful import Resource
from AmadeusAPIService import PNR as _PNR
from ...services import wrappers
from flask import session
from ...services.logger import operation_logger, error_logger
from ...config import config
from ...api.Ticket import parsers
import traceback


class IssueEMD(Resource):
    """ issue EMDs """

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):

        p = _PNR(credentials=session['amadeus_credentials'])
        args = parsers.issue_emd_parser.parse_args()
        try:
            tsm = [
                {
                    'type': 'TMT',
                    'value': _
                } for _ in args['tsm']
            ] if args['tsm'] is not None else None

            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])
            p.issue_consolidator_emd(references=tsm)
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'EMD(s) issued || PNR: {str(session["record_locator"])}')
            return {
                'status': True
            }
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'EMD(s) issuance unsuccessful || PNR: {str(session["record_locator"])} || ERROR: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            p.end_session()


class IssueETKT(Resource):
    """ issue ETKTs """

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):

        p = _PNR(credentials=session['amadeus_credentials'])
        args = parsers.issue_etkt_parser.parse_args()
        try:
            tst = [
                {
                    'type': 'TS',
                    'value': _
                } for _ in args['tst']
            ] if args['tst'] is not None else None
            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])
            p.issue_consolidator_etkt(references=tst)
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'ETKT(s) issued || PNR: {str(session["record_locator"])}')
            return {
                'status': True
            }
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'ETKT(s) issuance unsuccessful || PNR: {str(session["record_locator"])} || ERROR: {traceback.format_exc()}')

            queued = True
            try:
                p.place_pnr(
                    qnumber=config.ETKT_FAIL_QUEUE['qnumber'],
                    qcategory=config.ETKT_FAIL_QUEUE['qcategory'],
                    pnr=session["record_locator"],
                    oid=config.ETKT_FAIL_QUEUE['oid']  # remove if each agent has their own queue.
                )
                operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                    f'PNR queued due to ETKT issuance failure || PNR: {str(session["record_locator"])}')

            except Exception as e1:
                queued = False
                error_logger(__name__, credentials=session['amadeus_credentials']).error(
                    f'ETKT(s) issuance unsuccessful and failed to queue PNR || PNR: {str(session["record_locator"])} || ERROR: {traceback.format_exc()}')

            return {
                'status': False,
                'message': str(e),
                'queued': queued
            }
        finally:
            p.end_session()


class AutomaticUpdateETKT(Resource):
    """ Updates associated TST in preparation for reissue """

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        p = _PNR(credentials=session['amadeus_credentials'])
        args = parsers.auto_update_etkt_parser.parse_args()

        try:
            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])
            tst = p.automatic_etkt_update(args['etkt'], references=args['references'])
            p.rf_er(session['amadeus_credentials']['oid'] + session['amadeus_credentials']['username'])
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'ETKT Automatically Updated || PNR: {str(session["record_locator"])} || ETKT: {args["etkt"]}')
            return {
                'status': True,
                'tst': tst
            }
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Failed to Updated ETKT || PNR: {str(session["record_locator"])} || ETKT: {args["etkt"]} || Error: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            p.end_session()


class VoidDocument(Resource):
    """Void ETKT or EMD"""

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        p = _PNR(credentials=session['amadeus_credentials'])
        args = parsers.void_doc_parser.parse_args()

        try:
            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])
            p.cancel_document(doc_number=args['doc_number'], oid=session['credentials']['oid'])

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'Document {args["doc_number"]} voided successfully || PNR: {str(session["record_locator"])}')

            return {
                'status': True
            }

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Failed to void document {args["doc_number"]} || PNR: {str(session["record_locator"])} || Error: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }
        finally:
            p.end_session()


class VoidAllDocument(Resource):
    """Void all ETKTs and EMDs"""

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        p = _PNR(credentials=session['amadeus_credentials'])
        failed = []
        voided = []

        try:
            p.start_session(2)
            p.retrieve_by_rec_loc(session['record_locator'])
            ppnr = p.parse_pnr()
            for doc in ppnr['edoc']:
                try:
                    p.cancel_document(doc_number=doc['number'], oid=session['credentials']['oid'])
                    voided.append(doc['number'])
                    operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                        f'{doc["number"]} voided successfully || PNR: {str(session["record_locator"])}')

                except Exception as e:
                    failed.append({
                        'doc_number': doc['number'],
                        'error': str(e)
                    })
                    error_logger(__name__, credentials=session['amadeus_credentials']).error(
                        f'Failed to void {doc["number"]} || PNR: {str(session["record_locator"])} || Error: {traceback.format_exc()}')

            if len(voided) == 0:
                return {
                    'status': False,
                    'message': failed
                }
            else:
                return {
                    'status': True,
                    'voided': voided,
                    'failed': failed
                }

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Error while voiding documents || PNR: {str(session["record_locator"])} || Error: {traceback.format_exc()}')

            return {
                'status': False,
                'message': str(e)
            }
        finally:
            p.end_session()

