"""the api package will contain all application endpoints"""
from flask import send_from_directory, session, render_template, make_response
from flask_restful import Resource
import os
import datetime
from ..config import config
from ..services import wrappers

ROOT_DIRECTORY = os.path.dirname(os.path.dirname(__file__))


class Handover(Resource):
    """Handovers client to frontend interface"""

    def get(self):
        headers = {'Content-Type': 'text/html'}
        # return make_response(render_template('index.html'), 200, headers)
        return send_from_directory('../templates', 'index.html')


class SessionInformation(Resource):

    @wrappers.login_required
    def get(self):
        session_timeout = session['session_timeout']
        if session_timeout > datetime.datetime.utcnow():
            ttl = (session_timeout - datetime.datetime.utcnow()).seconds
            return {
                       'status': True,
                       'ttl': ttl
                   }, 200
        else:
            session.clear()
            return {
                       'status': False,
                       'message': 'Login required'
                   }, 401


class SessionReset(Resource):

    @wrappers.login_required
    def get(self):
        return {
                   'status': True
               }, 200


class DefaultCurrency(Resource):
    """Gets default currency of signed in OID"""

    @wrappers.login_required
    def get(self):
        return config.DEFAULT_CURRENCY(session['amadeus_credentials']['oid'])

