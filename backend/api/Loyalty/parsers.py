from flask_restful import reqparse

loyalty_profile__parsers = reqparse.RequestParser()
loyalty_profile__parsers.add_argument('miles_number', dest='miles_number', required=True, type=str)
