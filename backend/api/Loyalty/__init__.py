from flask_restful import Resource
from ...services import wrappers
from flask import session
from ...api.Loyalty import parsers
from ...services.logger import error_logger, operation_logger
from pyalms import ALMS
import traceback


class LoyaltyProfile(Resource):

    @wrappers.login_required
    def post(self):
        alms = ALMS()
        args = parsers.loyalty_profile__parsers.parse_args()
        try:
            profile = alms.retrieve_member_profile(args['miles_number'])

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(f'Loyalty profile retrieved || Miles Number: {args["miles_number"]}')

            return {
                'status': True,
                'profile': profile
            }, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(f'Failed to retrieve Loyalty profile || Miles Number: {args["miles_number"]} || Error: {traceback.format_exc()}')
            return {
                'status': False,
                'message': str(e)
            }, 400
