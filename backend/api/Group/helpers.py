from ...config import config
from AmadeusAPIService.utils.services import Services
from ...services.helpers import Helpers


class GroupHelpers(Helpers):

    def ffi_flights_combine(self, flights, fare_family_info):
        """returns city pair rb for each leg"""
        data = []
        for flight in flights:
            for st, leg in flight.items():
                data.append({
                    'origin': leg['departure_city'],
                    'destination': leg['arrival_city'],
                    'rbd': fare_family_info[st]['rbd'],
                    'st': st
                })
        return data

    def all_tsts_exist(self, group_size, tsts):
        """
        Check if all pax on PNR has an assigned TST
        :return:
        """
        num_tst = 0
        for tst in tsts:
            for ref in self._listify(tst['paxSegReference']['refDetails']):
                if 'P' in ref['refQualifier']:
                    num_tst += 1
        if group_size == num_tst:
            return True
        return False

    def get_depo_emd_info(self, PNR, svc_tattoo):
        for element in self._listify(PNR['Body']['PNR_Reply']['dataElementsMaster']['dataElementsIndiv']):
            if not self.dict_get(element, ['elementManagementData', 'segmentName'], '') == 'FA':
                continue
            references = self.dict_get(element, ['referenceForDataElement', 'reference'], [])
            for ref in self._listify(references):
                if ref['qualifier'] == 'ST' and ref['number'] == svc_tattoo:
                    data = element['otherDataFreetext']['longFreetext']
                    data = data[4:]
                    data = data.split('/')
                    emd = data[0]
                    issue_station = data[4][:3]
                    issue_date = data[3]
                    iata = data[5]
                    return {
                        'emd': emd,
                        'issue_station': issue_station,
                        'issue_date': issue_date,
                        'iata': iata
                    }
        raise Exception(f'No EMD found for SVC {svc_tattoo}')

    def fo_exist(self, tsmp):
        """ checks for the existence of the FO element in the TSMP"""
        for fare in self._listify(self.dict_get(tsmp, ['fareTattoos'], [])):
            if fare['referenceDetails']['type'] == 'FO':
                return True
        return False

    def tsmp_issued(self, tsmp):
        """returns true if the tsm is in issued state, else false"""
        for flag in self._listify(self.dict_get(tsmp, ['tsmFlags'], [])):
            if flag['statusDetails']['indicator'] == 'ISD':
                if flag['statusDetails']['action'] == '1':
                    return True
                else:
                    return False
        return False

