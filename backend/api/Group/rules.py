"""Business Rules Logic for Group Bookings"""
from ...config import config


class GroupBusinessRules:

    def pricing_by_info(self, combined_flight_info: list):
        """
        Fulfills restriction:

        No group discounts are allowed for the following
            On purchases of X, U, S and Business Class fares
            On Special/Sale Fares
            On the POS/TAB and TAB/POS sectors
        """
        discounted_segs = []
        for flight_info in combined_flight_info:
            if flight_info['rbd'] not in config.NO_DISCOUNT_CLASSES and \
                    not (flight_info['origin'] in config.DOMESTIC_CITIES and
                         flight_info['destination'] in config.DOMESTIC_CITIES):
                discounted_segs.append(flight_info['st'])

        if discounted_segs:
            return [
                {
                    'rate': ['GRP'],
                    'references': [
                        {
                            'type': 'S',
                            'value': _
                        } for _ in discounted_segs
                    ]
                }
            ]
        else:
            return None

    def pricing_by_ppnr(self, ppnr: dict):
        """
        Fulfills restriction:

        No group discounts are allowed for the following
            On purchases of X, U, S and Business Class fares
            On Special/Sale Fares
            On the POS/TAB and TAB/POS sectors
        """

        discounted_segs = []
        for st, details in ppnr['segments'].items():
            if details['bookingclass'] not in config.NO_DISCOUNT_CLASSES and \
                    not (details['origin'] in config.DOMESTIC_CITIES and
                         details['destination'] in config.DOMESTIC_CITIES):
                discounted_segs.append(st)

        if discounted_segs:
            return [
                {
                    'rate': ['GRP'],
                    'references': [
                        {
                            'type': 'S',
                            'value': _
                        } for _ in discounted_segs
                    ]
                }
            ]
        else:
            return None

    def deposit(self, ppnr):
        """
        Fulfills restriction:

        For travel to and from North America
            All groups must provide a deposit payment of USD 100 per person plus any applicable taxes

        For travel within the Caribbean
            All groups must provide a deposit payment of USD 50 per person plus any applicable taxes

        For travel between Trinidad and Tobago
            No deposits are necessary
        """

        cities = []
        group_size = int(ppnr['group_info']['size'])

        for st, details in ppnr['segments'].items():
            cities.append(details['origin'])
            cities.append(details['destination'])

        cities = list(set(cities))

        # check if cities are domestic only
        if set(cities) == set(config.DOMESTIC_CITIES):
            return group_size * config.DOMESTIC_DEPOSIT

        elif any([True for i in cities if i in config.NORTH_AMERICAN_CITIES]):
            return group_size * config.NORTH_AMERICAN_DEPOSIT

        else:
            return group_size * config.CARIBBEAN_DEPOSIT
