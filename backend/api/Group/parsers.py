from flask_restful import reqparse
from ...api.Group import argtypes

reduce_group_parser = reqparse.RequestParser(bundle_errors=True)
reduce_group_parser.add_argument('amount', dest='amount', required=True, type=int, help='Amount to reduce group size by')

increase_group_parser = reqparse.RequestParser(bundle_errors=True)
increase_group_parser.add_argument('amount', dest='amount', required=True, type=int, help='Amount to reduce group size by')

informative_group_price_parser = reqparse.RequestParser(bundle_errors=True)
informative_group_price_parser.add_argument('pricing_arguments', dest='pricing_arguments', required=True, type=dict)
informative_group_price_parser.add_argument('flights', dest='flights', required=True, type=dict, action='append')
informative_group_price_parser.add_argument('fare_family_info', dest='fare_family_info', required=True, type=dict)

price_group_parser = reqparse.RequestParser(bundle_errors=True)
price_group_parser.add_argument('pricing_arguments', dest='pricing_arguments', required=False, default={})

add_pax_parser = reqparse.RequestParser(bundle_errors=True)
add_pax_parser.add_argument('pax', dest='pax', required=False, default=None, type=dict, action='append', help='Passenger info')
add_pax_parser.add_argument('dob_info', dest='dob_info', required=False, default=None, type=dict, action='append', help='Currency to price PNR in')

create_group_parser = reqparse.RequestParser(bundle_errors=True)
create_group_parser.add_argument('group_name', dest='group_name', required=True, type=str, help='Group name')
create_group_parser.add_argument('group_size', dest='group_size', required=True, type=int, help='Number of passengers in group')
create_group_parser.add_argument('contact', dest='contact', required=True, type=dict, help='Group contact')
create_group_parser.add_argument('flights', dest='flights', required=True, type=dict, action='append', help='Flight segments')
create_group_parser.add_argument('group_leader', dest='group_leader', required=True, type=dict, help='Group leader')

validate_deposit_parser = reqparse.RequestParser(bundle_errors=True)
validate_deposit_parser.add_argument('deposit', dest='deposit', required=True, type=float, help='Deposit amount')