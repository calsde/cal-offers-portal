from flask_restful import Resource
from AmadeusAPIService.Group import GroupBooking
from AmadeusAPIService.Group.pnr_builder import GroupPNRBuilder
from AmadeusAPIService.utils.errorhandler import NoTSTRecord
from ...services import wrappers
from flask import session
from ...api.Group import parsers
from ...services.logger import error_logger, operation_logger
from ...config import config
from json import dumps
import traceback
from ...api.Group.helpers import GroupHelpers
from ...api.Group.rules import GroupBusinessRules
from AmadeusAPIService import AvailabilityPricer
import json


class BookingPossibility(Resource):

    @wrappers.login_required
    def post(self):
        gb = GroupPNRBuilder(credentials=session['amadeus_credentials'])
        args = parsers.informative_group_price_parser.parse_args()
        try:
            # get required arguments
            pricing_arguments = args['pricing_arguments']
            flights = args['flights']
            fare_family_info = args['fare_family_info']

            # calculate quantity
            quantity = 0
            for ptc, tattoos in pricing_arguments['passengers']:
                for tattoo in tattoos:
                    quantity += 1

            # check booking possibility
            flights = gb.search_create_pnr_converter(
                flights=flights,
                fare_family_info=fare_family_info,
                quantity=quantity
            )

            gb.start_session(2)

            poss = gb.check_booking_possibility(flights=flights, group_size=args['group_size'])

            response = {
                'status': True,
                'data': poss
            }

            if False in poss:
                operation_logger(__name__, credentials=session['amadeus_credentials']).info(f'Group booking impossible || Flight info: {dumps(args)}')
            else:
                operation_logger(__name__, credentials=session['amadeus_credentials']).info(f'Group booking possible || Flight info: {dumps(args)}')

            return response, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(f'Error in checking group booking possibility || Flight info: {dumps(args)} || ERROR: {traceback.format_exc()}')

            return {
                       'status': False,
                       'message': str(e)
                   }, 400
        finally:
            gb.end_session()


class InformativeGroupPrice(Resource, GroupBusinessRules, GroupHelpers):

    @wrappers.login_required
    def post(self):
        ap = AvailabilityPricer(credentials=session['amadeus_credentials'])
        args = parsers.informative_group_price_parser.parse_args()
        try:
            pricing_arguments = args['pricing_arguments']
            flights = args['flights']
            fare_family_info = args['fare_family_info']

            # add pricing discount codes to pricing arguments
            pricing_arguments['discount_codes'] = self.pricing_by_info(self.ffi_flights_combine(flights, fare_family_info))

            # add private fares
            pricing_arguments['private_fares'] = config.GROUP_PRIVATE_FARES

            ap.start_session(2)

            fares = ap.informative_pricer(
                flights=flights,
                fare_family_info=fare_family_info,
                pricing_arguments=pricing_arguments
            )

            # add fare family info to session for use later on
            session['pricing_info'] = {
                'flights': flights,
                'fare_family_info': fare_family_info
            }

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f'Informative Pricing || ARGS: {json.dumps(args)}')

            return {
                'status': True,
                'fares': fares
            }, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f'Informative Pricing Error || ARGS: {json.dumps(args)} || ERROR: {traceback.format_exc()}')
            return {
                       'status': False,
                       'message': str(e)
                   }, 400
        finally:
            ap.end_session()


class ReduceGroupSize(Resource):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):

        gb = GroupBooking(credentials=session['amadeus_credentials'])
        args = parsers.reduce_group_parser.parse_args()
        try:
            gb.start_session(2)
            gb.reduce_group_size(args['amount'], session['record_locator'], rf=session['amadeus_credentials']['oid'] + session['amadeus_credentials']['username'])
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(f'Group Reduced by {args["amount"]} || PNR: {session["record_locator"]}')

            return {
                'status': True
            }, 200

        except Exception as e:
            gb.end_session()
            error_logger(__name__, credentials=session['amadeus_credentials']).error(f'Error in checking group booking possibility || Flight info: {dumps(args)} || ERROR: {traceback.format_exc()}')

            return {
                'status': False,
                'message': str(e)
            }, 400
        finally:
            gb.end_session()


class IncreaseGroupSize(Resource):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):

        gb = GroupBooking(credentials=session['amadeus_credentials'])
        args = parsers.increase_group_parser.parse_args()
        try:
            gb.start_session(2)
            gb.retrieve_by_rec_loc(session['record_locator'])
            gb.increase_group_size(args['amount'])
            gb.rf(session['amadeus_credentials']['oid'] + session['amadeus_credentials']['username'])

            # check if increase is possible
            if gb.hk_checker():
                gb.er()
                operation_logger(__name__, credentials=session['amadeus_credentials']).info(f'Group increased by {args["amount"]} || PNR: {session["record_locator"]}')
                return {
                           'status': True
                       }, 200
            else:
                operation_logger(__name__, credentials=session['amadeus_credentials']).info(f'Not possible to increase group by {args["amount"]} in current booking class || PNR: {session["record_locator"]}')
                return {
                           'status': False,
                           'message': f'Not possible to increase group by {args["amount"]} in current booking class'
                       }

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(f'Error in increasing group size || PNR: {session["record_locator"]} || ERROR: {traceback.format_exc()}')

            return {
                'status': False,
                'message': str(e)
            }, 400
        finally:
            gb.end_session()


class CreateGroup(Resource, GroupHelpers):

    @wrappers.login_required
    @wrappers.required_in_session('fare_bases')
    def post(self):
        gpb = GroupPNRBuilder(credentials=session['amadeus_credentials'])
        args = parsers.create_group_parser.parse_args()
        try:
            existing_pricing_info = session['pricing_info']

            # parse flights
            flights = gpb.search_create_pnr_converter(
                flights=args['flights'],
                fare_family_info=existing_pricing_info['fare_family_info'],
                quantity=len(args['pax'])
            )

            gpb.start_session(2)
            gpb.add_group_name(args['group_name'], args['group_size'])
            gpb.add_contact(args['contact']['type'], args['contact']['value'])
            gpb.add_grpf(args['group_name'], args['group_size'])
            gpb.add_flight_segments(flights, bk_type='SG')
            gpb.add_pax([
                {
                    'last_name': args['group_leader']['lname'],
                    'first_name': args['group_leader']['fname'],
                    'type': args['group_leader']['type'],
                    'dob': args['group_leader']['dob'],
                    'identifier': '1'
                }
            ])
            gpb.add_docs(
                {
                    'dob': args['group_leader']['dob'],
                    'fname': args['group_leader']['fname'],
                    'lname': args['group_leader']['lname'],
                    'gender': args['group_leader']['gender'],
                    'reference': {'qualifier': 'PR', 'number': '1'}
                }
            )
            gpb.rf(session['amadeus_credentials']['oid'] + session['amadeus_credentials']['username'])
            gpb.tkok()
            gpb.option_code = 0
            gpb.create_pnr()

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(f'Group PNR created || PNR: {gpb.recloc}')

        except Exception as e:
            gpb.end_session()
            error_logger(__name__, credentials=session['amadeus_credentials']).error(f'Group PNR creation failure || Info: {dumps(args)} || ERROR: {traceback.format_exc()}')

            return {
                       'status': False,
                       'message': str(e)
                   }, 400

        gb = GroupBooking(credentials=session['amadeus_credentials'], session=gpb.session)

        try:

            ppnr = gb.parse_pnr(PNR=gpb.PNR)
            first_flight_info = gb._listify(gb._listify(args['flights'])[0]['itinerary'])[0]
            team_leader_tattoo = list(ppnr['passengers'].keys())[0]

            svc = gb.create_aux_segment('1', 'DEPO', first_flight_info['origin'], first_flight_info['dept_date'],
                                        pax_reference=[{'type': '1', 'value': team_leader_tattoo}],
                                        freetext=f"Group Deposit")

            tsmp = gb.create_manual_tsmp(service_references={'type': 'SVC', 'value': svc})

            # get ff info by flight info
            fare_family_info_by_flight = self.ffi_by_fi(existing_pricing_info['fare_family_info'], existing_pricing_info['flights'])

            # create new fare family info by segment tattoo
            fare_family_info = self.create_ffi(fare_family_info_by_flight, ppnr)

            # compress fare family info
            compressed_ffi = self.compress_ff_info(fare_family_info)

            # add custom remarks
            remarks = self.create_custom_remarks([
                {
                    "key": "SVC",
                    "value": svc
                },
                {
                    "key": "TSMP",
                    "value": tsmp
                },
                {
                    "key": "FFI",
                    "value": compressed_ffi
                }
            ])

            gb.add_remarks(remarks)

            gb.rf_er(session['amadeus_credentials']['oid'] + session['amadeus_credentials']['username'])
            operation_logger(__name__, credentials=session['amadeus_credentials']).info(f'Group DEPO TSMP set up successfully || PNR: {gb.recloc}')

            session['record_locator'] = gb.recloc

            return {
                'status': True,
                'record_locator': gb.recloc
            }, 200

        except Exception as e:
            gb.end_session()
            error_logger(__name__, credentials=session['amadeus_credentials']).error(f'Error in creating group DEPO TSMP || PNR: {gpb.recloc} || ERROR: {traceback.format_exc()}')

            return {
                       'status': False,
                       'message': str(e)
                   }, 400
        finally:
            gb.end_session()


class AddPax(Resource):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        gpb = GroupPNRBuilder(credentials=session['amadeus_credentials'])
        args = parsers.add_pax_parser.parse_args()
        try:
            gpb.start_session(2)
            gpb.retrieve_by_rec_loc(session['record_locator'])
            gpb.add_pax(args['pax'])
            gpb.add_docs(args['dob_info'])
            gpb.rf(session['amadeus_credentials']['oid'] + session['amadeus_credentials']['username'])
            gpb.update_pnr()

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(f"Passengers added to Group Booking || PNR: {session['record_locator']} || ARGS: {dumps(args)}")
            return {
                'status': True
            }, 200
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(f"Unable to add Passengers to PNR || PNR: {session['record_locator']} || ARGS: {dumps(args)} || ERROR: {traceback.format_exc()}")

            return {
                       'status': False,
                       'message': str(e)
                   }, 400
        finally:
            gpb.end_session()


class PriceGroupBooking(Resource, GroupBusinessRules, GroupHelpers):

    """ Get actual fare if all pax is added to the PNR else get estimate fare """

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        gb = GroupBooking(credentials=session['amadeus_credentials'])
        args = parsers.price_group_parser.parse_args()

        try:
            # get pricing arguments
            pricing_arguments = args['pricing_arguments']

            # get currency
            currency = self.dict_get(pricing_arguments, ['currency'], config.DEFAULT_CURRENCY(session['amadeus_credentials']['oid']))

            # update pricing args
            pricing_arguments['currency'] = currency

            gb.start_session(2)
            gb.retrieve_by_rec_loc(session['record_locator'])
            ppnr = gb.parse_pnr()

            # get fare family info
            custom_remarks = self.get_custom_remarks(gb.PNR, required_keys=['TSMP', 'FFI'])
            fare_family_info = self.decompress_ff_info(custom_remarks['FFI'])

            # add fare basis override to pricing arguments
            fbo = gb.create_fbo(fare_family_info)
            if 'fbo' not in pricing_arguments.keys():
                pricing_arguments['fbo'] = fbo

            # get required deposit
            required_deposit = self.deposit(ppnr)

            # calculate if currency does not match
            if currency.upper() != 'USD' and required_deposit != 0:
                required_deposit = gb.currency_converter('USD', currency.upper(), float(required_deposit))
                required_deposit = round(required_deposit['amount'], 2)
            
            # add pricing discount codes to pricing arguments
            pricing_arguments['discount_codes'] = self.pricing_by_ppnr(ppnr)

            # add private fares
            pricing_arguments['private_fares'] = config.GROUP_PRIVATE_FARES

            pricing = gb.price_group_booking(**pricing_arguments)

            # get coupon value
            tsmp = gb.get_tsmp(custom_remarks['TSMP'])

            downpayment_exist = gb.dict_get(tsmp, ['couponsDetails', 'couponValue', 'monetaryDetails'], False)

            if downpayment_exist:
                amount_paid = gb.dict_get(tsmp, ['couponsDetails', 'couponValue', 'monetaryDetails', 'amount'])
                amount_paid_currency = gb.dict_get(tsmp, ['couponsDetails', 'couponValue', 'monetaryDetails', 'currency'])

                if amount_paid_currency != currency:
                    gb.clear_session()
                    amount_paid = gb.currency_converter(amount_paid_currency, currency, amount_paid)

            else:
                amount_paid = 0

            # calc payable limit
            if pricing['fare_type'] == 'estimate':
                total = pricing['fare_info']['total_fare']['amount']
                payable_limit = config.GROUP_DOWNPAYMENT_LIMIT * total
            elif pricing['fare_type'] == 'actual':
                total = pricing['fare_info']['total_fare']['amount']
                payable_limit = total
            else:
                total = 0
                for tst, info in pricing['fare_info']['tst'].items():
                    total += info['total_fare']['amount']
                payable_limit = total

            # update session variables
            session['group_payable_limit'] = payable_limit
            session['group_amount_paid'] = amount_paid
            session['group_currency'] = currency
            session['group_priced'] = True

            # update pricing object
            pricing.update({
                'amount_paid': round(float(amount_paid), 2),
                'payable_limit': round(float(payable_limit), 2),
                'required_deposit': required_deposit
            })

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f"Group booking priced (Type: {pricing['fare_type']}) || PNR: {session['record_locator']}")

            return {
                'status': True,
                'pricing': pricing
            }

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f"Unable to price group booking || PNR: {session['record_locator']} || ERROR: {traceback.format_exc()}")

            return {
                       'status': False,
                       'message': str(e)
                   }, 400
        finally:
            gb.end_session()


class ValidateDeposit(Resource, GroupHelpers):

    """ At this stage, the fare of the booking is checked and the remaining fee
     is compared with the deposit amount """

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        gb = GroupBooking(credentials=session['amadeus_credentials'])
        args = parsers.validate_deposit_parser.parse_args()

        try:
            if 'group_priced' not in session:
                raise Exception('Group PNR must be priced')

            deposit = round(float(args['deposit']), 2)

            gb.start_session(2)
            gb.retrieve_by_rec_loc(session['record_locator'])
            rm_data = self.get_custom_remarks(gb.PNR, required_keys=['TSMP'])
            tsmp = gb.get_tsmp(rm_data['TSMP'])

            # check if initial deposit is at least the required amount

            if float(session['group_amount_paid']) > 0 and self.tsmp_issued(tsmp) == False:
                raise Exception('EMD issuance of previous deposit required.')

            if float(session['group_amount_paid']) + deposit > float(session['group_payable_limit']):
                raise Exception(f'Total deposit exceeds payable limit')

            else:
                # save validated deposit value in session
                session['group_depo'] = deposit

                # remove session vars that are no longer needed
                for i in ['group_priced', 'group_amount_paid', 'group_payable_limit']:
                    if i in session:
                        del session[i]

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f"Deposit Validated || PNR: {session['record_locator']}")

            return {
                'status': True
            }, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f"Deposit Validation Failed || PNR: {session['record_locator']} || ERROR: {traceback.format_exc()}")

            return {
                       'status': False,
                       'message': str(e)
                   }, 400
        finally:
            gb.end_session()


class ProcessDeposit(Resource, GroupHelpers):

    """ At this stage, the TSM is updated to reflect the deposit amount """

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        gb = GroupBooking(credentials=session['amadeus_credentials'])

        try:
            if 'group_depo' not in session:
                raise Exception('Deposit Validation Required')

            gb.start_session(2)
            gb.retrieve_by_rec_loc(session['record_locator'])
            rm_data = self.get_custom_remarks(gb.PNR, required_keys=['TSMP'])
            tsmp_tattoo = rm_data['TSMP']
            gb.update_depo_fare_info(tsmp_tattoo=tsmp_tattoo, deposit_amount=session['group_depo'],
                                     currency=session['group_currency'])
            gb.create_tsm_fare_element(qualifier='FE', tsm_tattoo=tsmp_tattoo, text='NON-REFUNDABLE')

            if not gb.first_issuance:
                emd_info = self.get_depo_emd_info(gb.PNR, rm_data['SVC'])
                rm_data = self.get_custom_remarks(gb.PNR, ['OEMD', 'OISTATION', 'OIDATE', 'OIATA', 'TSMP'])

                gb.update_fo_element(original_emd=rm_data['OEMD'],
                                     original_issue_station=rm_data['OISTATION'],
                                     original_issue_date=rm_data['OIDATE'],
                                     office_iata=rm_data['OIATA'],
                                     last_emd=emd_info['emd'],
                                     tsmp_tattoo=rm_data['TSMP'])

                gb.delete_existing_fp(rm_data['TSMP'])
                gb.fp_old(references=[{'type': 'TSM', 'value': rm_data['TSMP']}])

            gb.rf_er(session['amadeus_credentials']['oid'] + session['amadeus_credentials']['username'])

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f"Deposit Processed || PNR: {session['record_locator']} || DEPOSIT: {session['group_depo']}")

            session['first_depo_issuance'] = gb.first_issuance

            for i in ['group_depo', 'group_currency']:
                if i in session:
                    del session[i]

            return {
                       'status': True,
                       'tsmp_tattoo': rm_data['TSMP']
                   }, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f"Failed to process deposit|| PNR: {session['record_locator']} || ERROR: {traceback.format_exc()}")

            return {
                       'status': False,
                       'message': str(e)
                   }, 400
        finally:
            gb.end_session()


class IssueDepoEMD(Resource, GroupHelpers):

    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        gb = GroupBooking(credentials=session['amadeus_credentials'])

        try:

            gb.start_session(2)
            gb.retrieve_by_rec_loc(session['record_locator'])

            rm_data = self.get_custom_remarks(gb.PNR, ['TSMP', 'SVC'])

            gb.issue_consolidator_emd(references={'type': 'TMT', 'value': rm_data['TSMP']})

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                f"DEPO EMD Issued Successfully || PNR: {session['record_locator']}")

            try:
                if session.get('first_depo_issuance', False) is True:

                    # if this is the first DEPO EMD issuance, update the custom remarks
                    gb.retrieve_by_rec_loc(session['record_locator'])
                    emd_info = self.get_depo_emd_info(gb.PNR, rm_data['SVC'])

                    remarks = self.create_custom_remarks([
                        {
                            "key": "OEMD",
                            "value": emd_info["emd"]
                        },
                        {
                            "key": "OIDATE",
                            "value": emd_info["issue_date"]
                        },
                        {
                            "key": "OISTATION",
                            "value": emd_info["issue_station"]
                        },
                        {
                            "key": "OIATA",
                            "value": emd_info["iata"]
                        }
                    ])

                    gb.add_remarks(remarks)

                    gb.rf_er(session['amadeus_credentials']['oid'] + session['amadeus_credentials']['username'])
                    del session['first_depo_issuance']

                operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                    f"Remarks updated with EMD information || PNR: {session['record_locator']}")

            except Exception as e:
                error_logger(__name__, credentials=session['amadeus_credentials']).error(
                    f"Failed to update remarks ith EMD information || PNR: {session['record_locator']} || ERROR: {traceback.format_exc()}")

            return {
                'status': True
            }, 200

        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f"Failed to issue group EMD || PNR: {session['record_locator']} || ERROR: {traceback.format_exc()}")

            return {
                       'status': False,
                       'message': str(e)
                   }, 400
        finally:
            gb.end_session()


class CreateGroupTST(Resource, GroupBusinessRules, GroupHelpers):
    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        gb = GroupBooking(credentials=session['amadeus_credentials'])
        args = parsers.price_group_parser.parse_args()
        try:
            # get pricing arguments
            pricing_arguments = args['pricing_arguments']

            # get PNR
            gb.start_session(2)
            gb.retrieve_by_rec_loc(session['record_locator'])
            ppnr = gb.parse_pnr()

            group_size = int(ppnr['group_info']['size'])

            try:
                tsts = gb.get_all_tsts()
            except NoTSTRecord:
                tsts = []

            if self.all_tsts_exist(group_size, tsts):
                operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                    f"All group TSTs already exist || PNR: {session['record_locator']}")
                return {
                    'status': True
                }
            else:
                # get required remark data
                rm_data = self.get_custom_remarks(gb.PNR, required_keys=['FFI'])
                currency = self.dict_get(pricing_arguments, ['currency'], config.DEFAULT_CURRENCY(session['amadeus_credentials']['oid']))
                discount_pricing_codes = self.pricing_by_ppnr(ppnr)

                # get fbo
                fare_family_info = self.decompress_ff_info(rm_data['FFI'])

                # add fare basis override to pricing arguments
                fbo = gb.create_fbo(fare_family_info)
                if 'fbo' not in pricing_arguments.keys():
                    pricing_arguments['fbo'] = fbo

                # update pricing arguments
                pricing_arguments['currency'] = currency
                pricing_arguments['discount_codes'] = discount_pricing_codes
                pricing_arguments['private_fares'] = config.GROUP_PRIVATE_FARES

                pricing = gb.price_group_booking(**pricing_arguments)

                if pricing['fare_type'] == 'estimate':
                    raise Exception('All passengers must be added before booking is completed')
                else:
                    tsts = list(pricing['fare_info']['tst'].keys())
                    gb.create_tst_from_pricing(tsts)
                    gb.rf_er(session['amadeus_credentials']['oid'] + session['amadeus_credentials']['username'])
                    operation_logger(__name__, credentials=session['amadeus_credentials']).info(
                        f"Group TSTs created || PNR: {session['record_locator']}")
                    return {
                        'status': True
                    }
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(
                f"Unable to create group TSTs || PNR: {session['record_locator']} || ERROR: {traceback.format_exc()}")

            return {
                       'status': False,
                       'message': str(e)
                   }, 400
        finally:
            gb.end_session()


class AddGroupFOP(Resource, GroupHelpers):
    @wrappers.login_required
    @wrappers.pnr_required
    def post(self):
        gb = GroupBooking(credentials=session['amadeus_credentials'])
        try:
            gb.start_session(2)
            gb.retrieve_by_rec_loc(session['record_locator'])
            rm_data = self.get_custom_remarks(gb.PNR, ['TSMP', 'SVC'])

            parsed_pnr = gb.parse_pnr()
            group_size = int(parsed_pnr['group_info']['size'])

            # check if all pax has been added
            if len(parsed_pnr['passengers'].items()) == group_size:
                if not self.all_tsts_exist(group_size, tsts=gb.get_all_tsts()):
                    raise Exception('Group Pricing and TST Creation Required')
            else:
                raise Exception('All passengers must be added to PNR before adding FOP')

            # check if amount as been paid in FULL
            tsmp_tsttoo = rm_data['TSMP']
            depo_tsmp = gb.get_tsmp(tsmp_tsttoo)
            depo_coupon_value = depo_tsmp['couponsDetails']['couponValue']['monetaryDetails']['amount']
            depo_coupon_currency = depo_tsmp['couponsDetails']['couponValue']['monetaryDetails']['currency']

            tst_value = gb.sum_tsts(depo_coupon_currency)
            booking_fare = tst_value['total_fare']['amount']

            if round(float(depo_coupon_value), 2) != round(float(booking_fare), 2):
                raise Exception(f'Mismatch in deposit amount ({depo_coupon_value}) and booking fare ({booking_fare}).')

            emd_info = self.get_depo_emd_info(gb.PNR, rm_data['SVC'])
            emd_number = emd_info['emd'].split('-')[1]
            gb.add_group_fop(config.GROUP_FOP_AC, emd_number, session['amadeus_credentials']['oid'][:3])

            gb.rf_er(session['amadeus_credentials']['oid'] + session['amadeus_credentials']['username'])

            operation_logger(__name__, credentials=session['amadeus_credentials']).info(f"Group INV FOP added || PNR: {session['record_locator']}")
            return {
                'status': True
            }, 200
        except Exception as e:
            error_logger(__name__, credentials=session['amadeus_credentials']).error(f"Unable to add Group INV FOP || PNR: {session['record_locator']} || ERROR: {traceback.format_exc()}")

            return {
                       'status': False,
                       'message': str(e)
                   }, 400
        finally:
            gb.end_session()
