"""The model package will contain all of our database models"""

from ..config import config
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine(config.MySqlConnection.SQL_ALCHEMY_CONN, connect_args=config.MySqlConnection.SSL)
metadata = MetaData()
base = declarative_base(engine, metadata=metadata)


def create_session():
    sessions = sessionmaker(bind=engine, expire_on_commit=False, autoflush=False)
    return sessions()
