export const iataMapping: object = {

    ANU: {
      airportName: 'VC Bird International',
      country: 'Antigua and Barbuda',
      city: 'St. John\'s'
    },
    BGI: {
      airportName: 'Grantley Adams International',
      country: 'Barbados',
      city: 'Seawell, Christ Church'
    },
    CCS: {
      airportName: 'Simón Bolívar International',
      country: 'Venezuela',
      city: 'Caracas'
    },
    CUR: {
      airportName: 'Curaçao International',
      country: 'Curaçao',
      city: 'Willemstad'
    },
    FLL: {
      airportName: 'Fort Lauderdale-Hollywood International',
      country: 'Florida',
      city: 'Ft. Lauderdale'
    },
    GCM: {
      airportName: 'Owen Roberts International',
      country: 'Grand Cayman',
      city: 'Grand Cayman'
    },
    GEO: {
      airportName: 'Cheddi Jagan International',
      country: 'Guyana',
      city: 'Georgetown'
    },
    GND: {
      airportName: 'Maurice Bishop International',
      country: 'Grenada',
      city: 'St. George\'s'
    },
    HAV: {
      airportName: 'José Martí International',
      country: 'Cuba',
      city: 'Havana'
    },
    JFK: {
      airportName: 'John F Kennedy International',
      country: 'New York',
      city: 'New York'
    },
    KIN: {
      airportName: 'Norman Manley International',
      country: 'Jamaica',
      city: 'Kingston'
    },
    MBJ: {
      airportName: 'Jamaica',
      country: 'Sangster International',
      city: 'Montego Bay'
    },
    MCO: {
      airportName: 'Orlando International',
      country: 'Florida',
      city: 'Orlando'
    },
    MIA: {
      airportName: 'Miami International',
      country: 'Florida',
      city: 'Miami'
    },
    NAS: {
      airportName: 'Lynden Pindling International',
      country: 'Bahamas',
      city: 'Bahamas'
    },
    PBM: {
      airportName: 'Johan Adolf Pengel International',
      country: 'Suriname',
      city: 'Suriname'
    },
    POS: {
      airportName: 'Piarco International',
      country: 'Trinidad',
      city: 'Port-of-Spain'
    },
    SLU: {
      airportName: 'George FL Charles',
      country: 'St. Lucia',
      city: 'Castries'
    },
    SVD: {
      airportName: 'Argyle International',
      country: 'St. Vincent',
      city: 'Kingstown'
    },
    SXM: {
      airportName: 'Princess Juliana International',
      country: 'St. Maarten',
      city: 'St. Maarten'
    },
    TAB: {
      airportName: 'ANR Robinson International',
      country: 'Tobago',
      city: 'Tobago'
    },
    UVF: {
      airportName: 'Hewannora International',
      country: 'St. Lucia',
      city: 'Vieux Fort Quarter'
    },
    YYZ: {
      airportName: 'Toronto Pearson International\n',
      country: 'Canada',
      city: 'Toronto'
    }
  };
  
  export const PLACES = [
      'Afganistan', 'Albania', 'Algeria', 'American Samoa', 'Andorra',
      'Angola', 'Anguilla', 'Antigua & Barbuda', 'Argentina', 'Armenia',
      'Aruba', 'Australia', 'Austria', 'Azerbaijan',
      
      'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium',
      'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia', 'Bonaire',
      'Bosnia & Herzegovina', 'Botswana', 'Brazil', 'British Indian Ocean Ter',
      'Brunei', 'Bulgaria', 'Burkina Faso', 'Burundi',
      
      'Cambodia', 'Cameroon', 'Canada', 'Canary Islands', 'Cape Verde', 'Cayman Islands',
      'Central African Republic', 'Chad', 'Channel Islands', 'Chile', 'China',
      'Christmas Island', 'Cocos Island', 'Colombia', 'Comoros', 'Congo', 'Cook Islands',
      'Costa Rica', 'Cote DIvoire', 'Croatia', 'Cuba', 'Curaco', 'Cyprus', 'Czech Republic',
      
      'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic',
      
      'East Timor', 'Ecuador', 'Egypt', 'El Salvador',
      'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia',
      
      'Falkland Islands', 'Faroe Islands', 'Fiji', 'Finland', 'France',
      'French Guiana', 'French Polynesia', 'French Southern Ter',
      
      'Gabon', 'Gambia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar', 'Great Britain', 
      'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam',
      'Guatemala', 'Guinea', 'Guyana',
      
      'Haiti', 'Hawaii', 'Honduras', 'Hong Kong', 'Hungary',
      
      'Iceland', 'India', 'Indonesia', 'Iran', 'Iraq',
      'Ireland', 'Isle of Man', 'Israel', 'Italy',
      
      'Jamaica', 'Japan', 'Jordan',
      
      'Kazakhstan', 'Kenya', 'Kiribati', 'Korea North',
      'Korea Sout', 'Kuwait', 'Kyrgyzstan',
      
      'Laos', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia',
      'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg',
      
      'Macau', 'Macedonia', 'Madagascar', 'Malaysia', 'Malawi',
      'Maldives', 'Mali', 'Malta', 'Marshall Islands', 'Martinique',
      'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Midway Islands',
      'Moldova', 'Monaco', 'Mongolia', 'Montserrat', 'Morocco',
      'Mozambique', 'Myanmar',
      
      'Nambia', 'Nauru', 'Nepal', 'Netherland Antilles', 'Netherlands',
      'Nevis', 'New Caledonia', 'New Zealand', 'Nicaragua', 'Niger',
      'Nigeria', 'Niue', 'Norfolk Island', 'Norway',
      
      'Oman',
      
      'Pakistan', 'Palau Island', 'Palestine', 'Panama', 'Papua New Guinea',
      'Paraguay', 'Peru', 'Phillipines', 'Pitcairn Island', 'Poland',
      'Portugal', 'Puerto Rico',
      
      'Qatar',
      
      'Republic of Montenegro', 'Republic of Serbia', 'Reunion', 'Romania',
      'Russia', 'Rwanda',
      
      'St Barthelemy', 'St Eustatius', 'St Helena', 'St Kitts-Nevis',
      'St Lucia', 'St Maarten', 'St Pierre & Miquelon',
      'St Vincent & Grenadines', 'Saipan', 'Samoa', 'Samoa American',
      'San Marino', 'Sao Tome & Principe', 'Saudi Arabia', 'Senegal',
      'Serbia', 'Seychelles', 'Sierra Leone', 'Singapore', 'Slovakia',
      'Slovenia', 'Solomon Islands', 'Somalia', 'South Africa', 'Spain',
      'Sri Lanka', 'Sudan', 'Suriname', 'Swaziland', 'Sweden', 'Switzerland',
      'Syria',
      
      'Tahiti', 'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand', 'Togo',
      'Tokelau', 'Tonga', 'Trinidad & Tobago', 'Tunisia', 'Turkey',
      'Turkmenistan', 'Turks & Caicos Is', 'Tuvalu',
      
      'Uganda', 'Ukraine', 'United Arab Erimates', 'United Kingdom',
      'United States of America', 'Uraguay', 'Uzbekistan',
      
      'Vanuatu', 'Vatican City State', 'Venezuela', 'Vietnam',
      'Virgin Islands (Brit)', 'Virgin Islands (USA)',
      
      'Wake Island', 'Wallis & Futana Is',
      
      'Yemen',
      
      'Zaire', 'Zambia', 'Zimbabwe'
  ];
  
  export const FIRSTNAMES = [
      'Katia', 'Kaye', 'Sharyl',
      'Arlen', 'Tommie', 'Candi', 
      'Venice',  'Keitha',  'Carmon',  'Florentino'
  ];
  
  export const LASTNAMES = [
      'Danille', 'Tawanna', 'Abel',
      'Toni', 'Cary', 'Todd',
      'Verona', 'Vannesa', 'Man', 'Carl'
  ];
  
  export const TRAVEL_REASONS = ['Business', 'Pleasure', 'Studies'];
  
  export const STREETS = [
      'Hall Balk Lane', 'Heather Rowans', 'Sedwyn Street', 'Hollybush Terrace',
      'Bobby Fryer Close', 'Beech Tree Covert', 'Radnor Head',
      'Stephenson Meadow', 'Eagle Town', 'Oxford Covert'
  ];
  
  export const FLIGHT_CLASSES = [
      'Economy',
      'Business',
  ];
  
  export const BRANDED_FARE_CLASSES = [
    'Lite', 'Classic', 'Flex',
    'Biz', 'Biz Flex'
  ];
  
  export const BRANDED_FARE_ALLOWANCES = {
    'Lite': '0 bags free',
    'Classic': '1 bag free',
    'Flex': '2 bags free',
    'Biz': '2 bags free',
    'Biz Flex': '3 bags free'
  };
  
  export const MEALS = [
      'Saltfish',
      'Roti',
      'Dumpling',
      'Crab',
      'Doubles'
  ];
  
//   export const DANGEROUS_GOODS: DangerousGoodsCategory[] = [
//     {
//       title: 'Explosives',
//       items: [
//         'Fireworks', 'Loose Bullets', 'Automobile Airbags', 'Christmas Crackers',
//         'Party Poppers', 'Party Snaps', 'Gun Powder', 'Black Powder and substitutes'
//       ],
//       icon: 'explosive'
//     },
//     {
//       title: 'Radioactive Substances and Medicines',
//       items: [
//         'Radioactive Testing Sources'
//       ],
//       icon: 'radioactive'
//     },
//     {
//       title: 'Flammable Solids, Liquids, or Gases',
//       items: [
//         '151 Proof Rum', 'Acetone, Liquid Nails, Nail Polish', 'Nail Polish Remover',
//         'Tanks of gas (Power Tools)', 'Engine Parts', 'Canned Fuel', 'Isopropyl Alcohol',
//         'Bowling Ball Cleaners', 'Liquid Shoe Polish', 'Strike Anywhere Matches', 
//         'Solid Fuel Tablets', 'Model Rocket Engines', 'Charcoal with Starter Fuel',
//         'Construction Adhesives'
//       ],
//       icon: 'flammable',
//     },
//     {
//       title: 'Corrosive Substances',
//       items: [
//         'Automobiles', 'Motorcycles', 'Boats',
//         'All-Terrian Vehicles', 'Airplanes'
//       ],
//       icon: 'corrosive'
//     },
//     {
//       title: 'Oxidisers',
//       items: [
//         'Pool Shock', 'Bleach', 'Powdered Bleach', 'Powdered Cleansers', 'Chlorine Powder',
//         'Oxygen Cylinders', 'Oxygen Generators', 'Hydrogen Peroxide (greater than 8%)'
//       ],
//       icon: 'oxidize'
//     },
//     {
//       title: 'Poisons',
//       items: [
//         'Electroplating Solutions', 'Rat Bait', 'Rat Poison',
//         'Arsenic', 'Cyanide', 'Biological Substances'
//       ],
//       icon: 'poison'
//     }
//   ];
  
  const BUSINESS_COLUMNS: string[] = ['A', 'C', 'D', 'F'];
  const ECONOMY_COLUMNS: string[] = ['A', 'B', 'C', 'D', 'E', 'F'];
  
  export const BUSINESS_CABIN: SeatSection  = {
    startRow: 1,
    endRow: 3,
    columns: BUSINESS_COLUMNS,
  };
  
  export const CARIBBEAN_PLUS_CABIN: SeatSection = {
    startRow: 4,
    endRow: 10,
    columns: ECONOMY_COLUMNS,
  };
  
  export const ECONOMY_CABIN_BOEING: SeatSection = {
    startRow: 11,
    endRow: 26,
    columns: ECONOMY_COLUMNS
  };
  
  export const ECONOMY_CABIN_ATR: SeatSection = {
    startRow: 1,
    endRow: 17,
    columns: ECONOMY_COLUMNS
  }