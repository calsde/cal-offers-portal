import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  lastName: string;
  login = {
    record_locator: ''
  };
  constructor(private auth: AuthService, private router: Router) { }
  miles_login() {
    this.auth.retrievePNR(this.login).subscribe(
      response => {
        console.log(response);
        // if successful want to route to the seat selection page.
        this.router.navigateByUrl('/seat_select');
      },
      error => {
        console.log(error);
      }
    );
  }
  ngOnInit() {
    this.auth.authenticate().subscribe(
      response => {
        console.log(response);
      },
      err => {
        console.log(err);
      }
    );
  }

}
