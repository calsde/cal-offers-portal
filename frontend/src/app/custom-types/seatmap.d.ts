declare type Seat = string;

declare type SeatSection = {
    startRow: number;
    endRow: number;
    columns: string[];
    emergencyExitSeat?: string;
    rows?: number[];
};

declare type SeatMap = {
    'Business': SeatSection,
    'Caribbean Plus': SeatSection
};

