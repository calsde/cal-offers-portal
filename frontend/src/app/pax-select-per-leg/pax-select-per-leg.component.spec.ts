import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaxSelectPerLegComponent } from './pax-select-per-leg.component';

describe('PaxSelectPerLegComponent', () => {
  let component: PaxSelectPerLegComponent;
  let fixture: ComponentFixture<PaxSelectPerLegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaxSelectPerLegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaxSelectPerLegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
