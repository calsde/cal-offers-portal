import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {SeatService} from '../seat.service';

export interface PAX {
  id: number;
  name: string;
  seat: string;
  cost: number;
}

export interface PAX_LEG {
  id: number;
  origin: string;
  destination: string;
  PAX_LIST: PAX[];
  // leg_cost: number;
}

export interface PAX_LEGS {
  legs: PAX_LEG[];
}


@Component({
  selector: 'pax-select-per-leg',
  templateUrl: './pax-select-per-leg.component.html',
  styleUrls: ['./pax-select-per-leg.component.scss']
})
export class PaxSelectPerLegComponent implements OnInit {
  private selectedLeg: any;
  // expansionPanel: any;
  displayedColumns: string[];
  dataSource;
  total_sc: number;
  Passengers: PAX[];
  selectedPAX: string;
  totalLEG = 0;
  selectedSeats: string [];
  selectedRowIndex: number = -1;
  message: string;
  selectedSeatMapping: {};

  leg1: PAX_LEG = {
    id: 1,
    origin: 'TAB',
    destination: 'POS',
    PAX_LIST: [
      {id: 1, name: 'Tom', seat: '', cost: 100.00},
      {id: 2, name: 'Tim', seat: '10A', cost: 300.00},
      {id: 3, name: 'Trudy', seat: '', cost: 600.00},
    ],
    // leg_cost: )
  };


  leg2: PAX_LEG = {
    id: 2,
    origin: 'POS',
    destination: 'GEO',
    PAX_LIST: [
      {id: 1, name: 'Tom', seat: '4B', cost: 550.00},
      {id: 2, name: 'Tim', seat: '10A', cost: 800.00},
      {id: 3, name: 'Trudy', seat: '6D', cost: 135.00},
    ],
    // leg_cost: 1000.00
  };
  journey = [this.leg1, this.leg2];

  private leg: any;
  // totalOfLegs = this.journey.forEach(this.calcTotal);
  grandTotal: any;
  constructor(private seatFuncs: SeatService) {
    // this.Passengers = seatFuncs.getPassengers();
    this.dataSource = new MatTableDataSource<PAX>(this.Passengers);
    this.seatFuncs.seatitem$.subscribe(
      message => {
        this.message = message;
        if (this.selectedRowIndex > 0) {
          // This stores the selected seat into the position for the passenger being seated.
          this.journey[this.selectedLeg]['PAX_LIST'][this.selectedRowIndex - 1]['seat'] = message;
        }
        // console.log(this.message);
      },
      err => {
        console.log(err);
      },
    );
  }
  ngOnInit(): void {
    this.selectedSeatMapping = {};
    this.displayedColumns = ['id', 'name', 'cost'];
    this.total_sc = 1000;
    // Determine the total for the leg of the flight.
    this.journey.forEach(this.calcTotal);
    // Determine the total cost for the journey.
    this.grandTotal = this.journey.reduce(this.overallTotal);
    for (let leg of this.journey) {
      this.selectedSeats = [];
      console.log(leg);
      for (let pax of leg.PAX_LIST) {
        console.log(pax);
        if (pax.seat !== '') {
          // @ts-ignore
          this.selectedSeats.push(pax.seat);
        }
      }
      this.selectedSeatMapping[leg.id] = this.selectedSeats;
      console.log(this.selectedSeats);
    }
    console.log(this.selectedSeatMapping);
  }
  selectLeg(leg, panel) {
    console.log(leg);
    console.log(panel._expanded);
    if (panel._expanded) {
      console.log(this.selectedSeatMapping[leg.id]);
      for (let seat of this.selectedSeatMapping[leg.id]) {
        console.log(seat);
        document.getElementById(seat).classList.add('other-pax');
      }

    }
  }
  selectPAX(rowContent, row, leg) {
    console.log('Row ' + row);
    console.log('Leg ' + leg);
    this.selectedLeg = leg;
    if (this.selectedRowIndex === rowContent.id) { // Deselect when already selected.
      this.selectedRowIndex = -1;
      this.seatFuncs.deselectPAX(rowContent.seat);
    } else {
      this.selectedRowIndex = rowContent.id;
      this.seatFuncs.selectPAX(rowContent.seat, this.Passengers, leg, rowContent.id);
    }
  }
  calcTotal(value, index, array) {
    console.log(array);
    // this.selectedSeats.push(value.PAX_LIST.forEach(console.log()));
    // console.log(this.selectedSeats);
    value['total'] = value.PAX_LIST.map(l => l.cost).reduce((acc, valuee) => acc + valuee, 0);
    console.log(value);
    return;
  }
  overallTotal(total, value, index, array) {
    return total['total'] + value['total'];
  }
}
