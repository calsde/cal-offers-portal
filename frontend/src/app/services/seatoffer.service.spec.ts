import { TestBed } from '@angular/core/testing';

import { SeatofferService } from './seatoffer.service';

describe('SeatofferService', () => {
  let service: SeatofferService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SeatofferService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
