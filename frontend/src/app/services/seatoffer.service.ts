import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {API_URL} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SeatofferService {

  constructor(private http: HttpClient) {
  }
  getSeatMap(seatData) {
    /*
    * dept_date: string;
      dept_city: string;
      arrv_city: string;
      flight_number: number;
      booking_class: string;
      airline_code: string;
      currency: number;
  * */
    console.log(seatData);
    return this.http.post(API_URL + '/air/seatmap', {seatData});
  }
  getAssignedSeats() {
      return this.http.post(API_URL + '/ssr/get_assigned_seats', {});
  }
  cancelPnrElement(elementData) {
      return this.http.post(API_URL + '/pnr/cancel_element', elementData);
  }

  cancelPnrElements(elementData) {
      return this.http.post(API_URL + '/pnr/cancel_elements', elementData);
  }

  getAssignedServicesFlat() {
      return this.http.post(API_URL + '/ssr/get_assigned_services_indexed', {});
  }

  getAssignedSeatsFlat() {
      return this.http.post(API_URL + '/ssr/get_assigned_seats_indexed', {});
  }
}
