import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {API_URL} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private creds: { password: string; oid: string; username: string };

  constructor(private http: HttpClient) {}
  authenticate() {
    this.creds = {
      password: 'AmaWeb19',
      username: 'WSBWGEN',
      oid: 'POSBW0705'
    };
    return this.http.post( API_URL + '/auth/login', this.creds);
  }
  retrievePNR(recloc) {
    return this.http.post(API_URL + '/pnr/retrieve', recloc);
  }
}
