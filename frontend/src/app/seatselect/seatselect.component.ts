import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy, OnChanges, SimpleChanges } from '@angular/core';
import {
  BUSINESS_CABIN,
  CARIBBEAN_PLUS_CABIN,
  ECONOMY_CABIN_ATR,
  ECONOMY_CABIN_BOEING
} from 'src/utils/constants';
import { SeatofferService } from '../services/seatoffer.service';

@Component({
  selector: 'app-seatselect',
  templateUrl: './seatselect.component.html',
  styleUrls: ['./seatselect.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SeatselectComponent implements OnInit, OnChanges {
  // @Input() seatMap: SeatMap;
  // tslint:disable-next-line:no-output-rename
  @Output('seatSelected') seatEmitter = new EventEmitter<object>();
  panelOpenState = false;
  selectedSeat: string;

  businessCabin: SeatSection;
  caribbeanPlusCabin: SeatSection;
  // economyCabin: SeatSection;
  boeingFlight: boolean;

  constructor(private seatService: SeatofferService) {}
  message:string;

  receiveMessage($event) {
    this.message = $event;
  }


  // ===================================================================================================================================
  createArray(start: number, end: number) {
    return new Array(end - start + 1).fill(start).map((value, index) => value + index);
  }
  ngOnInit() {
    BUSINESS_CABIN.rows = this.createArray(BUSINESS_CABIN.startRow, BUSINESS_CABIN.endRow);
    CARIBBEAN_PLUS_CABIN.rows = this.createArray(CARIBBEAN_PLUS_CABIN.startRow, CARIBBEAN_PLUS_CABIN.endRow);
    this.businessCabin = BUSINESS_CABIN;
    this.caribbeanPlusCabin = CARIBBEAN_PLUS_CABIN;

    // Initiate the seat services.
    let seatMapReq = {
      dept_date: '010520',
      dept_city: 'POS',
      arrv_city: 'TAB',
      flight_number: '1502',
      booking_class: 'W',
      airline_code: 'BW',
      currency: 'TTD'
    };
    this.seatService.getSeatMap(seatMapReq).subscribe(
      response => {
        console.log(response);
      }
    );
    this.seatService.getAssignedSeats().subscribe(
      response => {
        console.log(response);
      }
    )
    /*{
    "status": true,
    "seat_map": {
        "flight_details": {
            "flight_number": "1502",
            "booking_class": "W",
            "off_point": "TAB",
            "board_point": "POS",
            "company": "BW",
            "flight_date": "010520",
            "flight_time": "0600"
        },
        "equipment_details": {
            "aircraft_type": "AT7",
            "cabin_details": [
                {
                    "cabin_class": "Y",
                    "capacity": 68
                }
            ]
        },
        "seat_details": {
            "exit_rows": [
                "1",
                "17"
            ],
            "rows": [
                {
                    "row_number": "1",
                    "columns": [
                        {
                            "column": "A",
                            "availability": "Z",
                            "characteristics": [
                                "1A",
                                "1B",
                                "E",
                                "IE",
                                "K",
                                "LS",
                                "W"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "B",
                            "availability": "Z",
                            "characteristics": [
                                "1A",
                                "1B",
                                "A",
                                "E",
                                "IE",
                                "K",
                                "LS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "C",
                            "availability": "Z",
                            "characteristics": [
                                "1A",
                                "1B",
                                "A",
                                "E",
                                "IE",
                                "K",
                                "RS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "D",
                            "availability": "Z",
                            "characteristics": [
                                "1A",
                                "1B",
                                "E",
                                "IE",
                                "K",
                                "RS",
                                "W"
                            ],
                            "chargeable": false
                        }
                    ]
                },
                {
                    "row_number": "2",
                    "columns": [
                        {
                            "column": "A",
                            "availability": true,
                            "characteristics": [
                                "I",
                                "LS",
                                "W",
                                "1"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "B",
                            "availability": "Z",
                            "characteristics": [
                                "A",
                                "I",
                                "LS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "C",
                            "availability": true,
                            "characteristics": [
                                "A",
                                "I",
                                "RS",
                                "1"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "D",
                            "availability": "Z",
                            "characteristics": [
                                "I",
                                "RS",
                                "W"
                            ],
                            "chargeable": false
                        }
                    ]
                },
                {
                    "row_number": "3",
                    "columns": [
                        {
                            "column": "A",
                            "availability": "Z",
                            "characteristics": [
                                "I",
                                "LS",
                                "W"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "B",
                            "availability": true,
                            "characteristics": [
                                "A",
                                "I",
                                "LS",
                                "1"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "C",
                            "availability": "Z",
                            "characteristics": [
                                "A",
                                "I",
                                "RS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "D",
                            "availability": true,
                            "characteristics": [
                                "I",
                                "RS",
                                "W",
                                "1"
                            ],
                            "chargeable": false
                        }
                    ]
                },
                {
                    "row_number": "4",
                    "columns": [
                        {
                            "column": "A",
                            "availability": true,
                            "characteristics": [
                                "I",
                                "LS",
                                "W",
                                "1"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "B",
                            "availability": "Z",
                            "characteristics": [
                                "A",
                                "I",
                                "LS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "C",
                            "availability": true,
                            "characteristics": [
                                "A",
                                "I",
                                "RS",
                                "1"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "D",
                            "availability": "Z",
                            "characteristics": [
                                "I",
                                "RS",
                                "W"
                            ],
                            "chargeable": false
                        }
                    ]
                },
                {
                    "row_number": "5",
                    "columns": [
                        {
                            "column": "A",
                            "availability": "Z",
                            "characteristics": [
                                "I",
                                "LS",
                                "W"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "B",
                            "availability": true,
                            "characteristics": [
                                "A",
                                "I",
                                "LS",
                                "1"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "C",
                            "availability": "Z",
                            "characteristics": [
                                "A",
                                "I",
                                "RS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "D",
                            "availability": true,
                            "characteristics": [
                                "I",
                                "RS",
                                "W",
                                "1"
                            ],
                            "chargeable": false
                        }
                    ]
                },
                {
                    "row_number": "6",
                    "columns": [
                        {
                            "column": "A",
                            "availability": true,
                            "characteristics": [
                                "I",
                                "LS",
                                "W",
                                "1"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "B",
                            "availability": "Z",
                            "characteristics": [
                                "A",
                                "I",
                                "LS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "C",
                            "availability": true,
                            "characteristics": [
                                "A",
                                "I",
                                "RS",
                                "1"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "D",
                            "availability": "Z",
                            "characteristics": [
                                "I",
                                "RS",
                                "W"
                            ],
                            "chargeable": false
                        }
                    ]
                },
                {
                    "row_number": "7",
                    "columns": [
                        {
                            "column": "A",
                            "availability": "Z",
                            "characteristics": [
                                "I",
                                "LS",
                                "W"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "B",
                            "availability": true,
                            "characteristics": [
                                "A",
                                "I",
                                "LS",
                                "1"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "C",
                            "availability": "Z",
                            "characteristics": [
                                "A",
                                "I",
                                "RS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "D",
                            "availability": true,
                            "characteristics": [
                                "I",
                                "RS",
                                "W",
                                "1"
                            ],
                            "chargeable": false
                        }
                    ]
                },
                {
                    "row_number": "8",
                    "columns": [
                        {
                            "column": "A",
                            "availability": true,
                            "characteristics": [
                                "I",
                                "LS",
                                "OW",
                                "W"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "B",
                            "availability": "Z",
                            "characteristics": [
                                "A",
                                "I",
                                "LS",
                                "OW"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "C",
                            "availability": true,
                            "characteristics": [
                                "A",
                                "I",
                                "OW",
                                "RS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "D",
                            "availability": "Z",
                            "characteristics": [
                                "I",
                                "OW",
                                "RS",
                                "W"
                            ],
                            "chargeable": false
                        }
                    ]
                },
                {
                    "row_number": "9",
                    "columns": [
                        {
                            "column": "A",
                            "availability": "Z",
                            "characteristics": [
                                "I",
                                "LS",
                                "OW",
                                "W"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "B",
                            "availability": true,
                            "characteristics": [
                                "A",
                                "I",
                                "LS",
                                "OW"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "C",
                            "availability": "Z",
                            "characteristics": [
                                "A",
                                "I",
                                "OW",
                                "RS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "D",
                            "availability": true,
                            "characteristics": [
                                "I",
                                "OW",
                                "RS",
                                "W"
                            ],
                            "chargeable": false
                        }
                    ]
                },
                {
                    "row_number": "10",
                    "columns": [
                        {
                            "column": "A",
                            "availability": true,
                            "characteristics": [
                                "I",
                                "LS",
                                "OW",
                                "W"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "B",
                            "availability": "Z",
                            "characteristics": [
                                "A",
                                "I",
                                "LS",
                                "OW"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "C",
                            "availability": true,
                            "characteristics": [
                                "A",
                                "I",
                                "OW",
                                "RS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "D",
                            "availability": "Z",
                            "characteristics": [
                                "I",
                                "OW",
                                "RS",
                                "W"
                            ],
                            "chargeable": false
                        }
                    ]
                },
                {
                    "row_number": "11",
                    "columns": [
                        {
                            "column": "A",
                            "availability": "Z",
                            "characteristics": [
                                "I",
                                "LS",
                                "OW",
                                "W"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "B",
                            "availability": true,
                            "characteristics": [
                                "A",
                                "I",
                                "LS",
                                "OW"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "C",
                            "availability": "Z",
                            "characteristics": [
                                "A",
                                "I",
                                "OW",
                                "RS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "D",
                            "availability": true,
                            "characteristics": [
                                "I",
                                "OW",
                                "RS",
                                "W"
                            ],
                            "chargeable": false
                        }
                    ]
                },
                {
                    "row_number": "12",
                    "columns": [
                        {
                            "column": "A",
                            "availability": true,
                            "characteristics": [
                                "I",
                                "LS",
                                "W"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "B",
                            "availability": "Z",
                            "characteristics": [
                                "A",
                                "I",
                                "LS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "C",
                            "availability": true,
                            "characteristics": [
                                "A",
                                "I",
                                "RS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "D",
                            "availability": "Z",
                            "characteristics": [
                                "I",
                                "RS",
                                "W"
                            ],
                            "chargeable": false
                        }
                    ]
                },
                {
                    "row_number": "13",
                    "columns": [
                        {
                            "column": "A",
                            "availability": "Z",
                            "characteristics": [
                                "I",
                                "LS",
                                "W"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "B",
                            "availability": true,
                            "characteristics": [
                                "A",
                                "I",
                                "LS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "C",
                            "availability": "Z",
                            "characteristics": [
                                "A",
                                "I",
                                "RS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "D",
                            "availability": true,
                            "characteristics": [
                                "I",
                                "RS",
                                "W"
                            ],
                            "chargeable": false
                        }
                    ]
                },
                {
                    "row_number": "14",
                    "columns": [
                        {
                            "column": "A",
                            "availability": true,
                            "characteristics": [
                                "DE",
                                "I",
                                "LS",
                                "W"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "B",
                            "availability": "Z",
                            "characteristics": [
                                "A",
                                "DE",
                                "I",
                                "LS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "C",
                            "availability": true,
                            "characteristics": [
                                "A",
                                "DE",
                                "I",
                                "RS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "D",
                            "availability": "Z",
                            "characteristics": [
                                "DE",
                                "I",
                                "RS",
                                "W"
                            ],
                            "chargeable": false
                        }
                    ]
                },
                {
                    "row_number": "15",
                    "columns": [
                        {
                            "column": "A",
                            "availability": "Z",
                            "characteristics": [
                                "I",
                                "LS",
                                "W"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "B",
                            "availability": true,
                            "characteristics": [
                                "A",
                                "I",
                                "LS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "C",
                            "availability": "Z",
                            "characteristics": [
                                "A",
                                "I",
                                "RS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "D",
                            "availability": true,
                            "characteristics": [
                                "I",
                                "RS",
                                "W"
                            ],
                            "chargeable": false
                        }
                    ]
                },
                {
                    "row_number": "16",
                    "columns": [
                        {
                            "column": "A",
                            "availability": true,
                            "characteristics": [
                                "I",
                                "LS",
                                "U",
                                "W"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "B",
                            "availability": "Z",
                            "characteristics": [
                                "A",
                                "I",
                                "LS",
                                "U"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "C",
                            "availability": true,
                            "characteristics": [
                                "A",
                                "I",
                                "RS",
                                "U"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "D",
                            "availability": "Z",
                            "characteristics": [
                                "I",
                                "RS",
                                "U",
                                "W"
                            ],
                            "chargeable": false
                        }
                    ]
                },
                {
                    "row_number": "17",
                    "columns": [
                        {
                            "column": "A",
                            "availability": "Z",
                            "characteristics": [
                                "1A",
                                "1B",
                                "E",
                                "IE",
                                "LS",
                                "W"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "B",
                            "availability": "Z",
                            "characteristics": [
                                "1A",
                                "1B",
                                "A",
                                "E",
                                "IE",
                                "LS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "C",
                            "availability": "Z",
                            "characteristics": [
                                "1A",
                                "1B",
                                "A",
                                "E",
                                "IE",
                                "RS"
                            ],
                            "chargeable": false
                        },
                        {
                            "column": "D",
                            "availability": "Z",
                            "characteristics": [
                                "1A",
                                "1B",
                                "E",
                                "IE",
                                "RS",
                                "W"
                            ],
                            "chargeable": false
                        }
                    ]
                }
            ]
        }
    },
    "seat_characteristics_description": {
        "1": "Restricted seat",
        "2": "Leg rest available",
        "3": "Individual video screen",
        "8": "No seat at this location",
        "9": "Center seat (not window, not aisle)",
        "1A": "Seat not available for infant",
        "1B": "Seat not available for medical",
        "1C": "Seat not available for unaccompanied minor",
        "1D": "Restricted reclined seat",
        "1M": "Seat with movie view",
        "1W": "Window seat without window",
        "3A": "Individual video screen - No choice of movie",
        "6A": "In front of galley seat",
        "6B": "Behind galley seat",
        "7A": "In front of toilet seat",
        "7B": "Behind toilet seat",
        "A": "Aisle",
        "AB": "Seat adjacent to bar",
        "AC": "Seat adjacent to closet",
        "AG": "Seat adjacent to galley",
        "AJ": "Adjacent aisle seat",
        "AL": "Seat adjacent to lavatory",
        "AM": "Individual movie screen - No choice of movie selection",
        "AS": "Individual airphone",
        "AT": "Seat adjacent to table",
        "AU": "Seat adjacent to stairs to upper deck",
        "B": "Seat with bassinet facility",
        "C": "Crew seat",
        "CH": "Chargeable seat",
        "DE": "Seat suitable for deportee",
        "E": "Exit and emergency exit",
        "EC": "Electronic connection for laptop or Fax machine",
        "EK": "Economy comfort seat",
        "H": "Seat with facility for handicapped/incapacitated passenger",
        "I": "Seat suitable for adult with infant",
        "IE": "Seat not suitable for child",
        "J": "Rear facing seat",
        "K": "Bulkhead seat",
        "KA": "Bulkhead seat with movie screen",
        "L": "Leg space seat",
        "LS": "Left side of aircraft",
        "M": "Seat without a movie view",
        "N": "Non smoking seat.\tCannot be defined in Altea Inventor",
        "O": "Preferential seat",
        "OW": "Overwing seat",
        "PC": "Pet cabin",
        "Q": "Seat in a quiet zone",
        "RS": "Right side of aircraft",
        "S": "Smoking seat",
        "U": "Seat suitable for unaccompanied minor",
        "UP": "Upper deck seat",
        "V": "Seat to be left vacant or last offered",
        "W": "Window seat",
        "WA": "Window and Aisle together",
        "X": "No facility seat (indifferent seat)"
    },
    "seat_occupation_description": {
        "F": "Free Seat",
        "G": "Group Seat",
        "O": "Seat occupied by passenger",
        "Z": "Seat occupied by Non Commercial PNR"
    }
}*/
  }

  ngOnChanges(change: SimpleChanges) {
    this.boeingFlight = true;
  }

}
