import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'caribbeanoffers';
  locay = '';
  path_object = {};
  constructor(private router: Router) {
    router.events.subscribe((val) => {
        this.locay = window.location.pathname;
        this.locay = window.location.pathname;
        this.path_object = {
          '/': window.location.pathname === '/',
          '/seat_select': window.location.pathname === '/seat_select',
          '/payment': window.location.pathname === '/payment',
          '/completed': window.location.pathname === '/completed',
        };
    });
  }
  ngOnInit() {
  }
}
