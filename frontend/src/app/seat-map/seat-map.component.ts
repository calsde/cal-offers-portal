import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import {
  BUSINESS_CABIN,
  CARIBBEAN_PLUS_CABIN
} from 'src/utils/constants';
import {SeatService} from '../seat.service';
import {BehaviorSubject, Observable, interval, fromEvent} from 'rxjs';

@Component({
  selector: 'seat-map',
  templateUrl: './seat-map.component.html',
  styleUrls: ['./seat-map.component.scss']
})

export class SeatMapComponent implements OnInit, OnChanges {
  // @Output('seatSelected') seatEmitter = new EventEmitter<object>();
  panelOpenState = false;
  selectedSeat: string;
  businessCabin: SeatSection;
  caribbeanPlusCabin: SeatSection;
  // economyCabin: SeatSection;
  boeingFlight: boolean;
  message: string;

  constructor(private seat_funcs: SeatService) {
    BUSINESS_CABIN.rows = this.createArray(BUSINESS_CABIN.startRow, BUSINESS_CABIN.endRow);
    CARIBBEAN_PLUS_CABIN.rows = this.createArray(CARIBBEAN_PLUS_CABIN.startRow, CARIBBEAN_PLUS_CABIN.endRow);
    this.businessCabin = BUSINESS_CABIN;
    // this.caribbeanPlusCabin = CARIBBEAN_PLUS_CABIN;
    this.caribbeanPlusCabin = this.seat_funcs.getCPCabin();
    console.log(this.caribbeanPlusCabin);
    this.seat_funcs.paxitem$.subscribe(
        message => {
          console.log(message);
          // this.message = 'Clicked the seat';
          console.log('Selected PAX Info');
        }
    );
  }

  createArray(start: number, end: number) {
    return new Array(end - start + 1).fill(start).map((value, index) => value + index);
  }

  ngOnInit() {

  }

  ngOnChanges(change: SimpleChanges) {
    this.boeingFlight = true;
    console.log('Changes ' + change);
  }

  clickedOnIcon(element: HTMLElement) {
    return element.tagName === 'I';
  }

  removeSelectedSeat() {
    try {
      console.log(document.getElementById(this.selectedSeat));
      (document.getElementById(this.selectedSeat).firstChild as HTMLElement).setAttribute('data-selected', 'false');
      this.seat_funcs.deselectSeat(this.selectedSeat);
    } catch (error) {
      console.log('No seat currently selected');
    }
  }

  markSelectedSeat(element: HTMLElement) {
    this.selectedSeat = element.id;
    // Include logic to change the selected seat or possible seat change for the PAX.
    // If one was already assigned then show the change (eg. 10A -> 5A) otherwise just show one.
    this.seat_funcs.selectSeat(this.selectedSeat, this.caribbeanPlusCabin, element);

  }

  selectSeat(target: HTMLElement) {
    if (this.clickedOnIcon(target)) {
      // If click on the same seat it should deselect the current seat.
      this.removeSelectedSeat();
      return;
    }

    this.removeSelectedSeat();
    // this.markSelectedSeat(target);
    this.selectedSeat = target.id;
    // Include logic to change the selected seat or possible seat change for the PAX.
    // If one was already assigned then show the change (eg. 10A -> 5A) otherwise just show one.
    this.seat_funcs.selectSeat(this.selectedSeat, this.caribbeanPlusCabin, target);
  }

}

