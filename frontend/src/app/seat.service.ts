import {Injectable, OnInit} from '@angular/core';
import {BehaviorSubject, Observable, interval, fromEvent} from 'rxjs';
import {
  BUSINESS_CABIN,
  CARIBBEAN_PLUS_CABIN
} from 'src/utils/constants';

export interface PAX {
  id: number;
  name: string;
  seat: string;
  cost: number;
}

@Injectable({
  providedIn: 'root'
})
export class SeatService implements OnInit {
  selectedSeat: string;
  pax_index: number;
  businessCabin: SeatSection;
  PAX_List: PAX[];
  caribbeanPlusCabin: SeatSection;
  // private paxSource = new BehaviorSubject<PAX>({id: 0, name: 'Test', seat: '0A', cost: 0});
  private paxSource = new BehaviorSubject<object>({leg: 0, position: 0, selected: false});
  // private seatSource = new BehaviorSubject<SeatSection>({startRow: 0, endRow: 10, columns: ['A','B','C'], emergencyExitSeat: "YES", rows: [1,2,3,4]});
  private seatSource = new BehaviorSubject<string>('');
  paxitem$ = this.paxSource.asObservable();
  seatitem$ = this.seatSource.asObservable();

  constructor() {
  }

  createArray(start: number, end: number) {
    return new Array(end - start + 1).fill(start).map((value, index) => value + index);
  }

  // tslint:disable-next-line:contextual-lifecycle
  ngOnInit() {

  }

  getCPCabin(): SeatSection {
    BUSINESS_CABIN.rows = this.createArray(BUSINESS_CABIN.startRow, BUSINESS_CABIN.endRow);
    CARIBBEAN_PLUS_CABIN.rows = this.createArray(CARIBBEAN_PLUS_CABIN.startRow, CARIBBEAN_PLUS_CABIN.endRow);
    this.businessCabin = BUSINESS_CABIN;
    this.caribbeanPlusCabin = CARIBBEAN_PLUS_CABIN;
    console.log(this.caribbeanPlusCabin);
    return this.caribbeanPlusCabin;
  }

  getPassengers(): PAX[] {
    /* Retrieve the list of passengers from API */
    return [
      {id: 1, name: 'Tom', seat: '', cost: 100.00},
      {id: 2, name: 'Tim', seat: '10A', cost: 300.00},
      {id: 3, name: 'Trudy', seat: '', cost: 600.00},
    ];
  }

  selectPAX(seat: string, passengerList: PAX[], selectedLeg: number, indexer: number) {
    /*
      This should accept the passenger clicked and highlight the seat on the seat map that
      indicating where the passenger currently is.
      As the seat is changed, the seat map with the adjustments should be returned.
      Consider logic to deselect the previously clicked passenger if the passenger is changed.
    */
    // console.log(passenger);
    // console.log(passengerList);
    // Changes the value of the observed variable to whatever is passed into it.
    this.paxSource.next({leg: selectedLeg, position: indexer - 1, selected: true});
    this.pax_index = indexer - 1;
    this.PAX_List = passengerList;
    if (seat) {
      // If this passenger has a seat assigned to them
      (document.getElementById(seat).firstChild as HTMLElement).setAttribute('data-selected', 'true');
      this.selectedSeat = seat;
      console.log('Passenger selected');
      console.log(this.selectedSeat);
    }
  }

  deselectPAX(passenger: string) {
    console.log(passenger);
    if (passenger && passenger.length > 0) {
      (document.getElementById(passenger).firstChild as HTMLElement).setAttribute('data-selected', 'false');
      this.selectedSeat = '';
      this.paxSource.next({leg: 0, position: 0, selected: false});
    }
  }

  selectSeat(seat: string, cabin: SeatSection, element: HTMLElement) {
    /*
      If a passenger is selected and a seat is selected then assign the seat to the passenger.
    */
    // console.log(seat);
    // console.log(cabin);
    // if document.getElementsByClassName()
    if (this.paxSource.value['selected']) {
      this.seatSource.next(seat);
      (element.firstChild as HTMLElement).setAttribute('data-selected', 'true');
      if (this.selectedSeat != null) {
        // Change the data for the seat map.
        console.log('Seat selected');
        console.log(this.PAX_List[this.pax_index]);
      } else {
        console.log('Passenger not selected');
      }
    } else {
      alert('Please select a Passenger first');
    }
  }
  deselectSeat(seat: string) {
    this.seatSource.next('');
    console.log(seat);
  }

}
