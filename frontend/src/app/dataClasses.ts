
export class SeatMap {
  dept_date: string;
  dept_city: string;
  arrv_city: string;
  flight_number: number;
  booking_class: string;
  airline_code: string;
  currency: number;
}
