import { BrowserModule } from '@angular/platform-browser';
import { NgModule, OnInit } from '@angular/core';
import { RouterModule, Routes, RoutesRecognized } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { MaterialSharedModule } from './material.module';
import { FormsModule } from '@angular/forms';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { SeatselectComponent } from './seatselect/seatselect.component';
import { PaxSelectPerLegComponent } from './pax-select-per-leg/pax-select-per-leg.component';
import { SeatMapComponent } from './seat-map/seat-map.component';
import { PaymentComponent } from './payment/payment.component';
import { SeatService } from './seat.service';
import { NgxPayPalModule } from 'ngx-paypal';
import { HttpClientModule } from '@angular/common/http';
const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'seat_select', component: SeatselectComponent},
  { path: 'payment', component: PaymentComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BreadcrumbsComponent,
    SeatselectComponent,
    PaxSelectPerLegComponent,
    SeatMapComponent,
    PaymentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialSharedModule,
    FormsModule,
    NgxPayPalModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: false, useHash: true
      }
    )
  ],
  providers: [SeatService],
  bootstrap: [AppComponent]
})
export class AppModule {
}


